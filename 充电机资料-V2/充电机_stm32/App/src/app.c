#include <stdint.h>
#include <string.h>
#include "key.h"
#include "usart.h"
#include "tim.h"
#include "charge_fsm.h"
#include "app.h"
#include "FaultCode.h"

/*
帧头(F)		命令(C)			数据长度(L)		数据区域(D)			CRC校验和(S)	帧尾(E)
1个字节		1个字节			1个字节			N个字节(小于255)	1个字节			1个字节
AA			0x1/0x02/03/04	L				data1 ... dataN		CRC				55
*/
/*累加和算法*/
uint8_t Cumulative_Sum(uint8_t  *buf, uint8_t  buf_len);

/*
先传低位，再传高位。
完整数据帧：
				   																					 
1. 电池充电：AA 01 06 B8 0B 74 0E 00 00  45 55
2. 操作完成：AA 02 06 00 00 00 00 00 00  00 55
3. 电池信息：AA 03 06 00 00 74 0E 00 00  82 55
4. 故障显示：AA 04 06 00 00 00 00 0F 00  0F 55
   故障显示：AA 04 06 00 00 00 00 00 10  01 55
*/
DEVICE_STATE g_refresh_device_state = {0};  	//定义结构体类型

/*功能：1.帧格式校验； 2.命令解析*/
void check_frame_format( )
{
	if(USART1_RX_STATE_FINISH == usart1_rx_state)  
	{			
		memset(&g_refresh_device_state, 0, sizeof(DEVICE_STATE));
		memcpy((uint8_t *)&g_refresh_device_state, g_usart1_rx_buff, sizeof(DEVICE_STATE));		
		
		if(usart1_rx_len < sizeof(DEVICE_STATE))
		{
			/*帧不完整，返回*/
			usart1_rx_state = USART1_RX_STATE_START;
			return ;
		}

		/*判断帧头*/
		if(0xAA != g_refresh_device_state.start_flag)
		{
			/*帧头不正确，返回*/
			usart1_rx_state = USART1_RX_STATE_START;
			return;
		}
					
		/*判断帧尾*/
		if(0x55 != g_refresh_device_state.end_flag)
		{
			/*帧尾不正确，返回*/
			usart1_rx_state = USART1_RX_STATE_START;
			return;
		}
		
		/*判断帧长度*/
		if(usart1_rx_len != sizeof(DEVICE_STATE))
		{
			usart1_rx_state = USART1_RX_STATE_START;
			//return;
		}
		
		/*判断校验和*/
		if( g_refresh_device_state.frame_sum != Cumulative_Sum((uint8_t *)&(g_refresh_device_state.battery_current), g_refresh_device_state.data_len) ) 
		{
			/*校验和没通过，返回*/
			usart1_rx_state = USART1_RX_STATE_START;
			return;
		}
				
		analyze_comm_data((DEVICE_STATE *)&g_refresh_device_state, sizeof(DEVICE_STATE));
		
		usart1_rx_state = USART1_RX_STATE_START;
		usart1_rx_len =0 ;
		memset(g_usart1_rx_buff, 0, USART1_MAX_RECV_LEN);
	}
}

/*命令解析*/
void analyze_comm_data(DEVICE_STATE *pDevice_State, uint8_t Size)
{
	uint8_t tmp_work_state = 0;
	uint16_t tmp_abnormal = 0;
	
	if((0 == pDevice_State) || (0 == Size))
	{
		return;
	}
	
	tmp_work_state = pDevice_State->cmd;
	tmp_abnormal = pDevice_State->abnormal_code;
		
	/*根据工作状态切换主状态机*/
	switch(tmp_work_state)
	{
		case CMD_STATE_BATTERY_CHARGE:
			 SET_EVENT(eBatChargeIng);					
		break;
		case CMD_STATE_OPERATE_FINISH:
			 SET_EVENT(eOperateFinish);	
		break;
		case CMD_STATE_INFO: //根据收到的帧数据中，设备类型切换到不同的状态。
			 SET_EVENT(eBatInfo);
		break;		
		case CMD_STATE_ABNORMAL:
			 SET_EVENT(eErrFaultCode);	
			 setFault(tmp_abnormal);			
		break;
		default:
		break;
		
	}
}

/*累加和算法*/
uint8_t Cumulative_Sum(uint8_t *buf, uint8_t  buf_len)
{
	uint8_t tmp_sum = 0;
	uint8_t i = 0;
	
	if((!buf) || (!buf_len))
	{
		return 0;
	}
	
	for(i=0;i<buf_len;i++)
	{
		tmp_sum += *(buf+i);
	}
	
	return tmp_sum;
}
