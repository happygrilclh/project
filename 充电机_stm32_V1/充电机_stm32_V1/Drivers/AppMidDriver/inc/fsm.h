#ifndef FSM_H
#define FSM_H

#define SET_FLAG				  1
#define CRC_FLAG				  0

#define BIT(x)  				  (1L << (x))
#define EVENT(id) 				  BIT(id)
#define checkEventID(event, id)   ((event) & EVENT(id))

volatile uint32 GET_EVENT(uint32 *p_g_event);
volatile void SET_EVENT(uint32 *p_g_event, uint32 event_id);

#endif