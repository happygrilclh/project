/**
  ******************************************************************************
  * File Name          : key.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "key.h"
#include "charge_fsm.h"

#define KEY_SELECT  HAL_GPIO_ReadPin(GPIOE, KEY_SELECT_Pin)
#define KEY_CONFIRM HAL_GPIO_ReadPin(GPIOE, KEY_CONFIRM_Pin)

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
     PA8   ------> RCC_MCO
*/
void KEY_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pins : PEPin PEPin */
  GPIO_InitStruct.Pin = KEY_SELECT_Pin|KEY_CONFIRM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct); 
}



void scan_key(void)
{
	if(!KEY_SELECT)
	{
		HAL_Delay(10);
		if(!KEY_SELECT)
		{
			while(!KEY_SELECT);
			SET_EVENT( eKeySelect);				 //给显示流程设置选择按键操作事件
		}
	}
	
	if(!KEY_CONFIRM)
	{
		HAL_Delay(10);
		if(!KEY_CONFIRM)
		{
			while(!KEY_CONFIRM);
			SET_EVENT( eKeyConfirm );				 //给显示流程设置选择按键操作事件
		}
	}
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
