#include "global.h"
#include "drive_oled.h"
#include "charge_fsm.h"
#include "FaultCode.h"

/*
开机界面的显示
*/
void display_start_log()
{
	Show_Str(44,12,128,24,(uint8*)"HELLO",16,1);
	Show_Str(37,32,128,12,(uint8*)"欢迎使用",12,1);
	Show_Str(24,52,128,12,(uint8*)"锂电池充电设备",12,1);
	OLED_Refresh_Gram();
}

/*
功能选择界面
*/
 void display_function(uint8_t select_num)	
{
	uint8_t tmp_sNum = select_num;
	uint8_t tmp_mode = 0x7;
	
	Show_Str(36,0,128,12,(uint8*)"功能选择",12,1);
	
	switch(tmp_sNum)
	{
		case STATE_SELECT_1:
			 tmp_mode = 0x1;
		break;
		case STATE_SELECT_2:
			 tmp_mode = 0x2;
		break;
		case STATE_SELECT_3:
			 tmp_mode = 0x4;		
		break;
		default:
		break;
		
	}
	Show_Str(8,15,128,12,(uint8*)"一、电池充电 ",12,!((tmp_mode>>0)&0x1));
	Show_Str(8,31,128,12,(uint8*)"二、电池信息 ",12,!((tmp_mode>>1)&0x1));
	Show_Str(8,47,128,12,(uint8*)"三、",12,!((tmp_mode>>2)&0x1));
	Show_Str(32,47,128,16,(uint8*)"ReadMe!",16,!((tmp_mode>>2)&0x1));
	OLED_Refresh_Gram();
}

/*
电池信息  
*/
void display_batt_info(void)
{
	Show_Str(36,0,128,24,(uint8*)"电池信息",12,1);
	Show_Str(0,15,128,12,(uint8*)"电池电压:",12,1);
	Show_Str(0,31,128,12,(uint8*)"充电次数:",12,1);
	Show_Str(0,47,128,12,(uint8*)"电池温度:",12,1);
	OLED_Refresh_Gram();
}

/*
电池正在充电
*/
void display_chargeIng(void)
{
	Show_Str(27,0,128,24,(uint8*)"正在充电...",12,1);
	Show_Str(0,20,128,12,(uint8*)"电池电压:",12,1);
	Show_Str(0,40,128,12,(uint8*)"充电电流:",12,1);
	OLED_Refresh_Gram();
}

/*
电池充电完成
*/
void display_charge_finish(void)
{
	Show_Str(33,0,128,24,(uint8*)"充电完成!",12,1);
	Show_Str(0,20,128,12,(uint8*)"电池电压:",12,1);
	OLED_Refresh_Gram();
}

/*
ReadMe : 软件信息
*/
void display_ver_info(void)
{
	
	Show_Str(0,0,128,12,(uint8*)"作者：happygril",12,1);
	Show_Str(0,15,128,12,(uint8*)"时间：2017/07/23",12,1);
	Show_Str(0,31,128,12,(uint8*)"软件版本号:V 2.0",12,1);
	Show_Str(0,48,128,12,(uint8*)"基于状态机的多级菜单",12,1);
	
	OLED_Refresh_Gram();
}

/*
显示故障信息
*/
void display_fault(void)
{
	Show_Str(20,0,128,12,(uint8*)"检测到故障！！！",12,1);
    faultCode_task();
	OLED_Refresh_Gram();
}

//==========================================================
/*按优先级，故障显示界面*/
void fault_disp(uint8_t line_num,uint32_t mode)
{
		
	switch(mode)
	{
		case 1: //输出短路
			Show_Str(8,(line_num*16),128,24,(uint8*)"输出短路",12,1);
			break;
		case 2://电池连接异常
			Show_Str(8,(line_num*16),128,24,(uint8*)"电池连接异常",12,1);
			break;
		case 3://充电电流过大
			Show_Str(8,(line_num*16),128,24,(uint8*)"充电电流过大",12,1);
			break;
		case 4://充电电流过小
			Show_Str(8,(line_num*16),128,24,(uint8*)"充电电流过小",12,1);
			break;
		case 5://充电电压过高
			Show_Str(8,(line_num*16),128,24,(uint8*)"充电电压过高",12,1);
			break;
		case 6://充电电压过低
			Show_Str(8,(line_num*16),128,24,(uint8*)"充电电压过低",12,1);
			break;
		case 7://电池温度过高 
			Show_Str(8,(line_num*16),128,24,(uint8*)"电池温度过高",12,1);
			break;
		case 8://电池温度过低 
			Show_Str(8,(line_num*16),128,24,(uint8*)"电池温度过低",12,1);;
			break;
		case 9://充电机过温
			Show_Str(8,(line_num*16),128,24,(uint8*)"充电机过温",12,1);
			break;
		case 10://充电通讯异常
			Show_Str(8,(line_num*16),128,24,(uint8*)"充电通讯异常",12,1);
			break;
		default:
			break;
	}
}


void drive_display(uint8 STATE)
{
	switch(STATE)
	{
		case STATE_POWER_ON:
			display_start_log();
		break;
		case STATE_SELECT_1:
			display_function(STATE_SELECT_1);
		break;
		case STATE_SELECT_2:
			display_function(STATE_SELECT_2);
		break;
		case STATE_SELECT_3:
			display_function(STATE_SELECT_3); 
		break;		
		case STATE_BATTERY_INFO:
			 display_batt_info();
		break;
		case STATE_BATTERY_CHARGE:
			display_chargeIng();
		break;
		case STATE_BATTERY_CHARGE_FINISH:
			display_charge_finish();
		break;		
		case STATE_VERSION_INFO:
			display_ver_info();
		break;			
		case STATE_FAULT_CODE:
			display_fault();			
		break;			
		default:
		break;
	}
}
