#ifndef CHARGE_FSM_H
#define CHARGE_FSM_H

#define BIT(x)  				  (1L << (x))
#define EVENT(id) 				  BIT(id)
#define checkEventID(event, id)   ((event) & EVENT(id))


extern volatile uint32_t g_fsm_disp_event;


//有可能在一个状态下，发生好几个事件。状态结束，是否结束事件
//不同的任务或者状态机，可以定义不同的事件集（enum，一个事件集最多32个事件）
//define the Task priority 
typedef enum
{
    eKeySelect = 0, 		//选择按键事件
    eKeyConfirm,    		//确认按键事件
	eBatChargeIng,			//电池标充电进行事件	
	eOperateFinish,			//电池充电完成事件, 电池修护完成事件, 电容充电完成事件
	eBatInfo,				//查看电池信息事件	
	eErrFaultCode,			//故障代码
}DispTaskEvent_t;

typedef enum
{	
	STATE_POWER_ON, 						//开机显示界面
	STATE_SELECT_1,						  	//行1 突出显示	
	STATE_SELECT_2,							//行2 突出显示
	STATE_SELECT_3,							//行3 突出显示
	STATE_BATTERY_CHARGE, 				    //电池标充电界面	
	STATE_BATTERY_CHARGE_FINISH,			//电池标充电完成
	STATE_BATTERY_INFO,                     //查看电池信息界面
	STATE_FAULT_CODE,					    //故障代码
	STATE_VERSION_INFO, 					//版本型号
}STATE;

typedef struct
{
	uint8_t  menu_root;  					//根目录菜单
	uint8_t  l_fsm_disp_state;				//oled屏显示的状态
	uint8_t  l_fsm_last_disp_state;			//oled屏显示的上一个状态
	uint8_t  notice_last_cmd;				//通知命令
}DispFsmPara;

void display_task(void);
void SET_EVENT(uint32_t event_id);
void  CRC_EVENT(uint32_t event_id);
uint32_t GET_EVENT(void);
void notice_cmd_data(uint8_t mode);
extern DispFsmPara g_disp_fsm_para;
#endif

