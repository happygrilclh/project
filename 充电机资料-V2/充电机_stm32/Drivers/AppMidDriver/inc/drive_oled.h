#ifndef __DRIVE_OLED_H
#define __DRIVE_OLED_H

void OLED_Init(void);
//清屏函数,清完屏,整个屏幕是黑色的!和没点亮一样!!!	  
void OLED_Clear(void);
//在指定位置开始显示一个字符串或者一行汉字		   		   
void Show_Str(uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t*str,uint8_t size,uint8_t mode);
//显示一个汉字
void Show_Font(uint16_t x,uint16_t y,uint8_t *font,uint8_t size,uint8_t mode);
//在指定位置显示一个字符,包括部分字符
void OLED_ShowChar(uint8_t x,uint8_t y,uint8_t chr,uint8_t size,uint8_t mode);
//显示字符串
void OLED_ShowString(uint8_t x,uint8_t y,const uint8_t *p,uint8_t size);
//显示数字
void LCD_ShowxNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len,uint8_t size,uint8_t mode);

//更新显存到LCD		 
void OLED_Refresh_Gram(void);
#endif 
