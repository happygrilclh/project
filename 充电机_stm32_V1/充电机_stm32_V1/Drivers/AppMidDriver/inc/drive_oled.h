#ifndef __DRIVE_OLED_H
#define __DRIVE_OLED_H

void OLED_Init(void);
//全屏清屏
void clear_full_screen(void);
void lcd_address(uint8_t page,uint8_t column);
//数据模式，写数据到OLED 显示模块
void transfer_data(int data1);
//命令模式，写命令到OLED 显示模块
void transfer_command(uint8_t data1);
#endif 
