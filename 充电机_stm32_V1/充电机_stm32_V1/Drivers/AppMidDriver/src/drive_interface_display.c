#include "global.h"
#include "drive_oled.h"
#include "charge_fsm.h"
#include "stdio.h"
#include "stdlib.h"
#include "zimo.h"

void display_graphic1_12x12(uint8_t page,uint8_t column,const uint8_t *dp,uint8_t mode)
{
	uint8_t i,j;
	for(j=0;j<2;j++)
	{
		lcd_address(page+j,column);
		for (i=0;i<12;i++)
		{	
			if(mode==1)
				transfer_data(*dp);		/*写数据到LCD,每写完一个8位的数据后列地址自动加1*/
			else if(mode==0)
				transfer_data(~*dp);	/*写数据到LCD,每写完一个8位的数据后列地址自动加1*/
			dp++;
		}
	}

}


//显示8x16 的点阵的字符串，括号里的参数分别为（页,列，字符串指针）
void display_string_8x16(uint8_t page,uint8_t column,const uint8_t *text)
{
	uint8_t i=0,j,k,n;
	
	if(column>123)
	{
		column=1;
		page+=2;
	}
	
	while(text[i]>0x00)
	{
		if((text[i]>=0x20)&&(text[i]<=0x7e))
		{
			j=text[i]-0x20;
			
			for(n=0;n<2;n++)
			{
				lcd_address(page+n,column);
				for(k=0;k<8;k++)
				{
					transfer_data(ascii_table_8x16[j][k+8*n]); //写数据到LCD,每写完1 字节的数据后列地址自动加1
				}
			}
			
			i++;
			column+=8;
		}
		else
		{
			i++;
		}
	}
}


/*
功能选择界面
*/
uint8 color_font=1;
 void display_function(uint8_t select_num)	
{
	color_font=1;
	display_graphic1_12x12(1,(1+12*3+8),gong1,color_font);	/*在第1页，第25列显示单个汉字"功"*/
	display_graphic1_12x12(1,(1+12*4+8),neng1,color_font); 	/*在第1页，第41列显示单个汉字"能"*/
	display_graphic1_12x12(1,(1+12*5+8),xuan1,color_font);	/*在第1页，第49列显示单个汉字"选"*/
	display_graphic1_12x12(1,(1+12*6+8),ze1,color_font); 	/*在第1页，第57列显示单个汉字"则"*/	   	    
	
	if(STATE_SELECT_1 == select_num)
	{
		color_font=0;
		display_graphic1_12x12(3,(1+8),shuzi1,color_font);	    
		display_graphic1_12x12(3,(1+12+8),dot1,color_font); 	
		display_graphic1_12x12(3,(1+12*2+8),dian1,color_font);	
		display_graphic1_12x12(3,(1+12*3+8),chi1,color_font); 	
		display_graphic1_12x12(3,(1+12*4+8),chong1,color_font);	
		display_graphic1_12x12(3,(1+12*5+8),dian1,color_font);	
		display_graphic1_12x12(3,(1+12*6+8),heiping,color_font);
		display_graphic1_12x12(3,(1+12*7+8),heiping,color_font);
		
		color_font=1; 
		display_graphic1_12x12(5,(1+8),shuzi2,color_font);	    
		display_graphic1_12x12(5,(1+12+8),dot1,color_font); 	
		display_graphic1_12x12(5,(1+12*2+8),dian1,color_font);	
		display_graphic1_12x12(5,(1+12*3+8),chi1,color_font); 	
		display_graphic1_12x12(5,(1+12*4+8),xin1,color_font);	
		display_graphic1_12x12(5,(1+12*5+8),xi1,color_font);	
		display_graphic1_12x12(5,(1+12*6+8),heiping,color_font);
		display_graphic1_12x12(5,(1+12*7+8),heiping,color_font);
		
		color_font=1; 
		display_graphic1_12x12(7,(1+8),shuzi3,color_font);	    
		display_graphic1_12x12(7,(1+12+8),dot1,color_font); 	
		display_graphic1_12x12(7,(1+12*2+8),she1,color_font);	
		display_graphic1_12x12(7,(1+12*3+8),bei1,color_font); 	
		display_graphic1_12x12(7,(1+12*4+8),xin1,color_font);	
		display_graphic1_12x12(7,(1+12*5+8),xi1,color_font);	
		display_graphic1_12x12(7,(1+12*6+8),heiping,color_font);
		display_graphic1_12x12(7,(1+12*7+8),heiping,color_font);

	}
	if(STATE_SELECT_2 == select_num)
	{
		color_font=1;
		display_graphic1_12x12(3,(1+8),shuzi1,color_font);	    
		display_graphic1_12x12(3,(1+12+8),dot1,color_font); 	
		display_graphic1_12x12(3,(1+12*2+8),dian1,color_font);	
		display_graphic1_12x12(3,(1+12*3+8),chi1,color_font); 	
		display_graphic1_12x12(3,(1+12*4+8),chong1,color_font);	
		display_graphic1_12x12(3,(1+12*5+8),dian1,color_font);	
		display_graphic1_12x12(3,(1+12*6+8),heiping,color_font);
		display_graphic1_12x12(3,(1+12*7+8),heiping,color_font);
		
		color_font=0; 
		display_graphic1_12x12(5,(1+8),shuzi2,color_font);	    
		display_graphic1_12x12(5,(1+12+8),dot1,color_font); 	
		display_graphic1_12x12(5,(1+12*2+8),dian1,color_font);	
		display_graphic1_12x12(5,(1+12*3+8),chi1,color_font); 	
		display_graphic1_12x12(5,(1+12*4+8),xin1,color_font);	
		display_graphic1_12x12(5,(1+12*5+8),xi1,color_font);	
		display_graphic1_12x12(5,(1+12*6+8),heiping,color_font);
		display_graphic1_12x12(5,(1+12*7+8),heiping,color_font);
		
		color_font=1; 
		display_graphic1_12x12(7,(1+8),shuzi3,color_font);	    
		display_graphic1_12x12(7,(1+12+8),dot1,color_font); 	
		display_graphic1_12x12(7,(1+12*2+8),she1,color_font);	
		display_graphic1_12x12(7,(1+12*3+8),bei1,color_font); 	
		display_graphic1_12x12(7,(1+12*4+8),xin1,color_font);	
		display_graphic1_12x12(7,(1+12*5+8),xi1,color_font);	
		display_graphic1_12x12(7,(1+12*6+8),heiping,color_font);
		display_graphic1_12x12(7,(1+12*7+8),heiping,color_font);
	}		
	if(STATE_SELECT_3 == select_num)
	{
		color_font=1;
		display_graphic1_12x12(3,(1+8),shuzi1,color_font);	    
		display_graphic1_12x12(3,(1+12+8),dot1,color_font); 	
		display_graphic1_12x12(3,(1+12*2+8),dian1,color_font);	
		display_graphic1_12x12(3,(1+12*3+8),chi1,color_font); 	
		display_graphic1_12x12(3,(1+12*4+8),chong1,color_font);	
		display_graphic1_12x12(3,(1+12*5+8),dian1,color_font);	
		display_graphic1_12x12(3,(1+12*6+8),heiping,color_font);
		display_graphic1_12x12(3,(1+12*7+8),heiping,color_font);
		
		color_font=1; 
		display_graphic1_12x12(5,(1+8),shuzi2,color_font);	    
		display_graphic1_12x12(5,(1+12+8),dot1,color_font); 	
		display_graphic1_12x12(5,(1+12*2+8),dian1,color_font);	
		display_graphic1_12x12(5,(1+12*3+8),chi1,color_font); 	
		display_graphic1_12x12(5,(1+12*4+8),xin1,color_font);	
		display_graphic1_12x12(5,(1+12*5+8),xi1,color_font);	
		display_graphic1_12x12(5,(1+12*6+8),heiping,color_font);
		display_graphic1_12x12(5,(1+12*7+8),heiping,color_font);
		
		color_font=0; 
		display_graphic1_12x12(7,(1+8),shuzi3,color_font);	    
		display_graphic1_12x12(7,(1+12+8),dot1,color_font); 	
		display_graphic1_12x12(7,(1+12*2+8),she1,color_font);	
		display_graphic1_12x12(7,(1+12*3+8),bei1,color_font); 	
		display_graphic1_12x12(7,(1+12*4+8),xin1,color_font);	
		display_graphic1_12x12(7,(1+12*5+8),xi1,color_font);	
		display_graphic1_12x12(7,(1+12*6+8),heiping,color_font);
		display_graphic1_12x12(7,(1+12*7+8),heiping,color_font);			
	}
}

/*
电池信息  
*/
void display_batt_info(void)
{
	color_font=1; 
	display_graphic1_12x12(2,(1+8),dian1,color_font); 	
	display_graphic1_12x12(2,(1+12*1+8),chi1,color_font);	
	display_graphic1_12x12(2,(1+12*2+8),xin1,color_font); 
	display_graphic1_12x12(2,(1+12*3+8),xi1,color_font);

	display_graphic1_12x12(4,(1+8),shuzi2,color_font); 	   
	display_graphic1_12x12(4,(1+12*1+8),ji1,color_font);
	display_graphic1_12x12(4,(1+12*2+8),cai1,color_font);   
	display_graphic1_12x12(4,(1+12*3+8),dan1,color_font);	
}

/*
电池正在充电
*/
void display_chargeIng(void)
{
	color_font=1; 
	display_graphic1_12x12(2,(1+8),dian1,color_font); 	
	display_graphic1_12x12(2,(1+12*1+8),chi1,color_font);	
	display_graphic1_12x12(2,(1+12*2+8),dian1,color_font); 
	display_graphic1_12x12(2,(1+12*3+8),ya1,color_font);

	display_graphic1_12x12(4,(1+8),shuzi2,color_font); 	   
	display_graphic1_12x12(4,(1+12*1+8),ji1,color_font);
	display_graphic1_12x12(4,(1+12*2+8),cai1,color_font);   
	display_graphic1_12x12(4,(1+12*3+8),dan1,color_font);	
}


/*
ReadMe : 软件信息
*/
void display_ver_info(void)
{
	uint8_t num[10];
	color_font=1; 
	
	display_graphic1_12x12(2,(1+8),ruan1,color_font);		
	display_graphic1_12x12(2,(1+12*1+8),jian1,color_font); 
	display_graphic1_12x12(2,(1+12*2+8),ban1,color_font);	
	display_graphic1_12x12(2,(1+12*3+8),ben1,color_font); 		
	display_graphic1_12x12(2,(1+12*4+8),hao1,color_font); 		
	display_graphic1_12x12(2,(1+12*5+8),maohao1,color_font); 
	display_string_8x16(2,(1+12*6+4),(uint8_t *)"V");
	
	sprintf((char *)num,"%3.1f",(float)(1.0));
	display_string_8x16(2,(1+12*7+8),&num[0]);
	display_string_8x16(2,(1+12*7+16),&num[1]);
	display_string_8x16(2,(1+12*7+24),&num[2]);
	display_string_8x16(2,(1+12*7+32),&num[3]);

	display_graphic1_12x12(4,(1+8),shuzi2,color_font); 	   
	display_graphic1_12x12(4,(1+12*1+8),ji1,color_font);
	display_graphic1_12x12(4,(1+12*2+8),cai1,color_font);   
	display_graphic1_12x12(4,(1+12*3+8),dan1,color_font);	
}



void drive_display(uint8 STATE)
{
	switch(STATE)
	{
		case STATE_SELECT_1:
			display_function(STATE_SELECT_1);
		break;
		case STATE_SELECT_2:
			display_function(STATE_SELECT_2);
		break;
		case STATE_SELECT_3:
			display_function(STATE_SELECT_3); 
		break;		
		case STATE_BATTERY_INFO:
			 display_batt_info();
		break;
		case STATE_BATTERY_CHARGE:
			display_chargeIng();
		break;		
		case STATE_VERSION_INFO:
			display_ver_info();
		break;						
		default:
		break;
	}
}
