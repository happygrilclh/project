#ifndef _FAULTCODE_H
#define _FAULTCODE_H

#define FAULTCODE_NUM 				9 //异常代码数量

#define BIT(x)  (1L << (x))
#define mExistCode(tcode,codeID)  	(tcode & BIT(codeID))
#define mExistFault(faultID) 		 mExistCode(baFaultCode.all,faultID)
#define mNotExistFault(faultID) 	(!mExistCode(baFaultCode.all,faultID))
#define mSetBit(tcode, codeID) 		 tcode |= (BIT(codeID))
#define mSetFaultCode(faultID) 		 mSetBit(baFaultCode.all,faultID)
#define mClrBit(tcode,codeID)		 tcode &= ~(BIT(codeID))
#define mClrFaultCode(faultID) 		 mClrBit(baFaultCode.all, faultID)

typedef struct
{
  uint8_t fault_output_shortCircuit:1;			//输出短路	       /*1 */
  uint8_t fault_batts_connect_NG:1;				//电池连接异常     /*2 */ 
  uint8_t fault_charge_current_HIGH:1;			//充电电流过大	   /*3 */
  uint8_t fault_charge_current_LOW:1;			//充电电流过小	   /*4 */
  uint8_t fault_charge_volt_HIGH:1;				//充电电压过高	   /*5*/
  uint8_t fault_charge_volt_LOW:1;				//充电电压过低	   /*6*/
  uint8_t fault_batts_temp_HIGH:1;				//电池组温度过高   /*7*/
  uint8_t fault_batts_temp_LOW:1;				//电池温度过低     /*8*/
  uint8_t fault_charge_comm_NG:1;				//充电通讯异常	   /*9*/			      
}faultCode_t;

typedef union
{
	uint32_t all;
	faultCode_t fCode;
}fault_t;

typedef enum
{
  fault_output_shortCircuit,			//输出短路	       /*1 */
  fault_batts_connect_NG,				//电池组连接异常   /*2 */ 
  fault_charge_current_HIGH,			//充电电流过大	   /*3 */
  fault_charge_current_LOW,			    //充电电流过小	   /*4 */
  fault_charge_volt_HIGH,				//充电电压过高	   /*5*/
  fault_charge_volt_LOW,				//充电电压过低	   /*6*/
  fault_batts_temp_HIGH,				//电池组温度过高   /*7*/
  fault_batts_temp_LOW,				    //电池组温度过低   /*8*/
  fault_charge_comm_NG,				    //充电通讯异常	   /*9*/			      
}faultCodeE_t;


//显示故障任务
void faultCode_task(void);
//设置故障
void setFault(uint32_t faultCode);

#endif 

