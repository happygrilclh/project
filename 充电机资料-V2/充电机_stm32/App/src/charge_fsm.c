#include "global.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "usart.h"
#include "charge_fsm.h"
#include "drive_oled.h"
#include "tim.h"
#include "app.h"

volatile uint32_t g_fsm_disp_event = 0x00; //最多32个事件


static DispFsmPara g_disp_fsm_para = 
{
	STATE_SELECT_1,
	STATE_POWER_ON,
	STATE_SELECT_1,
	0
};

extern void drive_display(uint8 STATE);

uint32_t GET_EVENT(void)
{
	uint32_t tmp_event = g_fsm_disp_event;
	
	g_fsm_disp_event = 0;
	
	return tmp_event;
}


/*
设置事件，是设置总事件的某一位。
读取事件，将事件读取后，要将该事件位清除，表示已处理掉该位的事件。
*/
void  SET_EVENT(uint32_t event_id)
{
	g_fsm_disp_event |= BIT(event_id);
}

/*
清除事件，清除总事件的某一位。
*/
void  CRC_EVENT(uint32_t event_id)
{
	g_fsm_disp_event &= ~BIT(event_id);
}


void display_task(void)
{
	volatile uint32_t tmp_event = 0;
	
	tmp_event = GET_EVENT();
		
	switch(g_disp_fsm_para.l_fsm_disp_state)
	{
		
		case STATE_POWER_ON:
			drive_display(STATE_POWER_ON);
			HAL_Delay(2500);   							
			g_disp_fsm_para.l_fsm_disp_state = STATE_SELECT_1;	
			OLED_Clear();		
		break;
		case STATE_SELECT_1:
			drive_display(STATE_SELECT_1); //行1 突出显示
		    g_disp_fsm_para.menu_root = STATE_SELECT_1;
		
			if(checkEventID(tmp_event, eKeySelect))
			{
				g_disp_fsm_para.l_fsm_disp_state = STATE_SELECT_2;
				return; //每次一个状态下，只能处理一个事件，处理完此事件后，要把事件清0，否则有可能处理多次事件，引起逻辑错误。
			}

			
			if(checkEventID(tmp_event, eKeyConfirm))
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = STATE_BATTERY_CHARGE;
				return;
			}
			
			
		break;
		case STATE_SELECT_2:
			drive_display(STATE_SELECT_2); //行2 突出显示
		    g_disp_fsm_para.menu_root = STATE_SELECT_2;
		
			
			if(checkEventID(tmp_event, eKeySelect))
			{
				g_disp_fsm_para.l_fsm_disp_state = STATE_SELECT_3;
				return;
			}
		
			if(checkEventID(tmp_event, eKeyConfirm))
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = STATE_BATTERY_INFO;
				return;					
			}
			
		break;
		case STATE_SELECT_3:
			drive_display(STATE_SELECT_3); //行3 突出显示
		    g_disp_fsm_para.menu_root = STATE_SELECT_3;
		
		
			if(checkEventID(tmp_event, eKeySelect))
			{
				//OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = STATE_SELECT_1;
				return;
			}
			
			if(checkEventID(tmp_event, eKeyConfirm))
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = STATE_VERSION_INFO;
				return;
						
			}
			
		break;
		case STATE_BATTERY_INFO:
			 drive_display(STATE_BATTERY_INFO);
		     g_disp_fsm_para.l_fsm_last_disp_state = STATE_BATTERY_INFO;
		
#if defined(TEST_MODE)				
			Show_Str(60,15,128,16,(uint8*)" 3.75",16,1);
			Show_Str(110,15,128,16,(uint8*)"V",16,1);
			LCD_ShowxNum(90,31,21,2,16,1);
			Show_Str(110,31,128,12,(uint8*)"次",12,1);
			Show_Str(60,47,128,16,(uint8*)" 25.6",16,1);
			Show_Str(110,47,128,12,(uint8*)"℃",12,1);
#endif
			if(checkEventID(tmp_event, eBatInfo))
			{
				/*假数据*/ 
				//太懒了，这部分用假数据。
				Show_Str(60,15,128,16,(uint8*)" 3.75",16,1);
				Show_Str(110,15,128,16,(uint8*)"V",16,1);
				LCD_ShowxNum(90,31,21,2,16,1);
				Show_Str(110,31,128,12,(uint8*)"次",12,1);
				Show_Str(60,47,128,16,(uint8*)" 25.6",16,1);
				Show_Str(110,47,128,12,(uint8*)"℃",12,1);
				return;
			}

			
			if(checkEventID(tmp_event, eKeySelect))
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = STATE_SELECT_2;
				return;
			}
							
		break;
		case STATE_BATTERY_CHARGE:
			 drive_display(STATE_BATTERY_CHARGE);

			 g_disp_fsm_para.l_fsm_last_disp_state = STATE_BATTERY_CHARGE;
		
			if(checkEventID(tmp_event, eKeySelect))
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = STATE_SELECT_1;
				return;
			}
						
			if(checkEventID(tmp_event, eOperateFinish))
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = STATE_BATTERY_CHARGE_FINISH;
				return;
			}
			
#if defined(TEST_MODE)				
			Show_Str(60,20,128,16,(uint8*)"3.75",16,1);
			Show_Str(110,20,128,16,(uint8*)"V",16,1);
			Show_Str(60,40,128,16,(uint8*)"4.25",16,1);
			Show_Str(110,40,128,16,(uint8*)"A",16,1);		
#endif			
			if(checkEventID(tmp_event, eBatChargeIng))
			{
				//显示数据
				char num[10];				
				
				/*通讯口过来的真实数据 正式*/
				sprintf(num,"%5.2f",(float)(g_refresh_device_state.battery_voltage * 0.001));
				Show_Str(60,20,128,16,(uint8*)num,16,1);
				Show_Str(110,20,128,16,(uint8*)"V",16,1);
				
				sprintf(num,"%5.2f",(float)(g_refresh_device_state.battery_current * 0.001));
				Show_Str(60,40,128,16,(uint8*)num,16,1);
				Show_Str(110,40,128,16,(uint8*)"A",16,1);
				return;
			}
		
			if(checkEventID(tmp_event, eErrFaultCode))
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = STATE_FAULT_CODE;
				return;
			}
	
				
		break;
		case STATE_VERSION_INFO :	//版本界面
			 drive_display(STATE_VERSION_INFO);	 
			 g_disp_fsm_para.l_fsm_last_disp_state = STATE_VERSION_INFO; //????
		   	
			
			if(checkEventID(tmp_event, eKeySelect))
			{
					OLED_Clear();
					g_disp_fsm_para.l_fsm_disp_state = STATE_SELECT_3;
				    return;
			}
			
		break;
		case STATE_BATTERY_CHARGE_FINISH:	
			g_disp_fsm_para.l_fsm_last_disp_state = STATE_BATTERY_CHARGE_FINISH;		
		
			drive_display(STATE_BATTERY_CHARGE_FINISH);
			
			if(checkEventID(tmp_event, eKeySelect))
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = g_disp_fsm_para.menu_root;
				return;
			}

			if(checkEventID(tmp_event, eBatChargeIng))
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = STATE_BATTERY_CHARGE;
				return;
			}
			
			if(checkEventID(tmp_event, eOperateFinish))
			{
				//显示数据 太懒了，这部分不想实现了。
				return;
			}
			
		break;
		case STATE_FAULT_CODE:			//故障
			 drive_display(STATE_FAULT_CODE);
		    
			if( (checkEventID(tmp_event, eKeySelect)) || checkEventID(tmp_event, eKeyConfirm) )
			{
				OLED_Clear();
				g_disp_fsm_para.l_fsm_disp_state = g_disp_fsm_para.menu_root;
				return;
			}
			break;
		default:
		break;	
	}
}
