#ifndef __SSD1309_H
#define __SSD1309_H

#define OLED_DC_Pin GPIO_PIN_3
#define OLED_DC_GPIO_Port GPIOC

#define OLED_RST_Pin GPIO_PIN_4
#define OLED_RST_GPIO_Port GPIOC

void OLED_GPIO_init(void);

#endif 

