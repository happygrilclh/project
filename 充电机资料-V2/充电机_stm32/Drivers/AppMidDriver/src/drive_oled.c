#include "global.h"
#include "string.h"
#include "spi.h"
#include "oled_ssd1309.h"
#include "oled_Font.h"
#include "drive_oled.h"

#define OLED_HEIGHT		 128
#define OLED_WIDTH		 64
#define GB16_LEN		 32+2
#define GB12_LEN 		 24+2

typedef enum
{
	OLED_CMD = 1,
	OLED_DATA = 3,
}OLED_WR_TYPE;

//=========================================================
//OLED的显存
//存放格式如下.
//[0]0 1 2 3 ... 127	
//[1]0 1 2 3 ... 127	
//[2]0 1 2 3 ... 127	
//[3]0 1 2 3 ... 127	
//[4]0 1 2 3 ... 127	
//[5]0 1 2 3 ... 127	
//[6]0 1 2 3 ... 127	
//[7]0 1 2 3 ... 127 

uint8_t OLED_GRAM[128][8];



/*
函数声明
*/
//从汉字字库中查找出字模
void Get_HzMat(uint8_t *code,uint8_t *mat,uint8_t size);
//画点 
void OLED_DrawPoint(uint8_t x,uint8_t y,uint8_t t);

/*
写一个字节
*/
void OLED_WR_Byte(uint8_t dat, uint8_t cmd)
{
	if(OLED_CMD == cmd)      //命令模式，写命令到OLED 显示模块
	{
		HAL_GPIO_WritePin(GPIOC, OLED_DC_Pin, GPIO_PIN_RESET);
		SPI_Write(dat);
	}
	else if(OLED_DATA == cmd) //数据模式，写数据到OLED 显示模块
	{
		HAL_GPIO_WritePin(GPIOC, OLED_DC_Pin, GPIO_PIN_SET);
		SPI_Write(dat);	
	}
	else
	{
		
	}
}

//更新显存到LCD		 
void OLED_Refresh_Gram(void)
{
	uint8_t i,n;		    
	for(i=0;i<8;i++)  
	{  
		OLED_WR_Byte (0xb0+i,OLED_CMD);    //设置页地址（0~7）
		OLED_WR_Byte (0x00,OLED_CMD);      //设置显示位置—列低地址
		OLED_WR_Byte (0x10,OLED_CMD);      //设置显示位置—列高地址   
		for(n=0;n<128;n++)OLED_WR_Byte(OLED_GRAM[n][i],OLED_DATA); 
	}   
}

//清屏函数,清完屏,整个屏幕是黑色的!和没点亮一样!!!	  
void OLED_Clear(void)  
{ 	
	uint8_t i,n; 
	
	for(i=0;i<8;i++)
	{
		for(n=0;n<128;n++)
		{
			OLED_GRAM[n][i]=0X00;  
		}
	}
	
	OLED_Refresh_Gram();//更新显示
}

void OLED_Init(void)		 
{
	OLED_GPIO_init();
	
	OLED_WR_Byte(0xFD,OLED_CMD);	//Command Lock  ( SSD1309 use, SSD1305 no use )
	OLED_WR_Byte(0x12,OLED_CMD);	// Unlock Driver IC (0x12/0x16)	   
	OLED_WR_Byte(0xAE,OLED_CMD);	// Set Display Off
	OLED_WR_Byte(0xD5,OLED_CMD);	//Set Display Clock Divide Ratio/Oscillator Frequency	  设置时钟分频
	OLED_WR_Byte(0xA0,OLED_CMD);	//
	OLED_WR_Byte(0xA8,OLED_CMD);	//Set Multiplex Ratio 	   设置多路复用比率
	OLED_WR_Byte(0x3F,OLED_CMD);	// 
	OLED_WR_Byte(0xD3,OLED_CMD);	//Set Display Offset
	OLED_WR_Byte(0x00,OLED_CMD);	// 
	OLED_WR_Byte(0x40,OLED_CMD);	// Set Display Start Line
    OLED_WR_Byte(0xA1,OLED_CMD);	// Set SEG/Column Mapping (0xA0/0xA1)
	OLED_WR_Byte(0xC8,OLED_CMD);	// Set COM/Row Scan Direction (0xC0/0xC8)
	OLED_WR_Byte(0xDA,OLED_CMD);	//Set COM Pins Hardware Configuration
	OLED_WR_Byte(0x12,OLED_CMD);	// Set Alternative Configuration (0x02/0x12)	
	OLED_WR_Byte(0x81,OLED_CMD);	//Set Current Control 
    OLED_WR_Byte(0xFF,OLED_CMD);    // 	
	OLED_WR_Byte(0xD9,OLED_CMD);	//Set Pre-Charge Period 
	OLED_WR_Byte(0x82,OLED_CMD);	// 
	OLED_WR_Byte(0xDB,OLED_CMD);	//Set VCOMH Deselect Level
	OLED_WR_Byte(0x34,OLED_CMD);	// 

    //OLED_WR_Byte(0x20,OLED_CMD);
    //OLED_WR_Byte(0x02,OLED_CMD);	// Set Page Addressing Mode (0x00/0x01/0x02)

	OLED_WR_Byte(0xA4,OLED_CMD);	// Set Entire Display On/Off (0xA4/0xA5)
	OLED_WR_Byte(0xA6,OLED_CMD);	// Set Normal/Inverse Display (0xA6/0xA7)

	OLED_Clear(); //清屏
	HAL_Delay(10);
	
	OLED_WR_Byte(0xAF,OLED_CMD);		//Set Display On
	HAL_Delay(200);
}





//在指定位置开始显示一个字符串或者一行汉字	    
//支持自动换行
//(x,y):起始坐标
//width,height:区域
//str  :字符串
//size :字体大小
//mode:0,非叠加方式;1,叠加方式    	   		   
void Show_Str(uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t*str,uint8_t size,uint8_t mode)
{					
	uint16_t x0=x;
	uint16_t y0=y;							  	  
    uint8_t bHz=0;     //字符或者中文  
	
    while(*str!=0)//数据未结束
    { 
        if(!bHz) 
        {
	        if(*str>0x80)
			{
				bHz=1;//中文 
			}
	        else      //字符
	        {      
                if(x>(x0+width-size/2))//换行
				{				   
					y+=size;
					x=x0;	   
				}	
				
		        if(y>(y0+height-size))
				{
					break;//越界返回   
				}
				
		        if(*str==13)//换行符号
		        {         
		            y+=size;
					x=x0;
		            str++; 
		        }  
		        else 
				{
					OLED_ShowChar(x,y,*str,size,mode);//有效部分写入  
				}
				
				str++; 
		        x+=size/2; //字符,为全字的一半 
	        }
        }
		else//中文 
        {     
            bHz=0;//有汉字库    
            if(x>(x0+width-size))//换行
			{	    
				y+=size;
				x=x0;		  
			}
	        if(y>(y0+height-size))
			{
				break;//越界返回 
			}
			
	        Show_Font(x,y,str,size,mode); //显示这个汉字,空心显示 
	        str+=2; 
	        x+=size;//下一个汉字偏移	    
        }						 
    }   
}  	



//code 字符指针开始
//从字库中查找出字模
//code 字符串的开始地址,GBK码
//mat  数据存放地址 (size/8+((size%8)?1:0))*(size) bytes大小	
//size:字体大小
void Get_HzMat(uint8_t *code,uint8_t *mat,uint8_t size)
{		    
	uint8_t qh,ql;
	uint8_t tmp_GBH,tmp_GBL;
	uint8_t i;					  
	
	uint8_t csize=(size/8+((size%8)?1:0))*(size);//得到字体一个字符对应点阵集所占的字节数
	
	qh = *code;
	ql = *(++code);


	if(16 == size)
	{
		for(i=0;i<(sizeof(GB_16)/GB16_LEN);i++)
		{
			tmp_GBH = GB_16[i].Index[0];
			tmp_GBL = GB_16[i].Index[1];
			if((qh == tmp_GBH) && (ql == tmp_GBL))
			{
				memcpy(mat,GB_16[i].Msk,csize);				
			}
		}
	}
	else if(12 == size)
	{
		for(i=0;i<(sizeof(GB_12)/GB12_LEN);i++)
		{
			tmp_GBH = GB_12[i].Index[0];
			tmp_GBL = GB_12[i].Index[1];
			if((qh == tmp_GBH) && (ql == tmp_GBL))
			{
				memcpy(mat,GB_12[i].Msk,csize);				
			}
		}
	}
	else
	{
		
	}
	
    												    
} 

//显示一个指定大小的汉字
//x,y :汉字的坐标
//font:汉字GBK码
//size:字体大小
//mode:0,正常显示,1,叠加显示	   
void Show_Font(uint16_t x,uint16_t y,uint8_t *font,uint8_t size,uint8_t mode)
{
	uint8_t temp,t,t1;
	uint16_t y0=y;
	uint8_t dzk[128];   
	uint8_t csize=(size/8+((size%8)?1:0))*(size);			//得到字体一个字符对应点阵集所占的字节数	
	
	if((size != 12) && (size != 16))
	{
		return;	//不支持的size
	}
	
	Get_HzMat(font,dzk,size);	//得到相应大小的点阵数据 
	
	for(t=0;t<csize;t++)
	{   												   
		temp=dzk[t];			//得到点阵数据                          
		for(t1=0;t1<8;t1++)
		{
			if(temp&0x80)
			{
				OLED_DrawPoint(x,y,mode);
			}
			else //if(mode==0)
			{
				OLED_DrawPoint(x,y,!mode); 
			}
			
			temp<<=1;
			y++;
			if((y-y0)==size)
			{
				y=y0;
				x++;
				break;
			}
		}  	 
	}  
}

//在指定位置显示一个字符,包括部分字符
//x:0~127
//y:0~63
//mode:0,反白显示;1,正常显示				 
//size:选择字体 12/16/24
void OLED_ShowChar(uint8_t x,uint8_t y,uint8_t chr,uint8_t size,uint8_t mode)
{      			    
	uint8_t temp,t,t1;
	uint8_t y0=y;
	uint8_t csize=(size/8+((size%8)?1:0))*(size/2);		//得到字体一个字符对应点阵集所占的字节数 (size16:csize16)(size24:csize36)
	chr=chr-' ';//得到偏移后的值	
	
    for(t=0;t<csize;t++)
    {   
		if(size==16)
		{
			temp=Asc_1608[chr][t];		//调用1608字体
		}
		else if(size==12)
		{	
			temp=Asc_1206[chr][t];		//调用2412字体
		}
		else 
		{	
			return;						//没有的字库
		}
		
        for(t1=0;t1<8;t1++)
		{
			if(temp&0x80)
			{	
				OLED_DrawPoint(x,y,mode);
			}
			else 
			{
				OLED_DrawPoint(x,y,!mode);
			}
			
			temp<<=1;
			y++;
			if(y>=OLED_WIDTH)
			{
				return;		//超区域了
			}
			
			if((y-y0)==size)
			{
				y=y0;
				x++;
				if(x>=OLED_HEIGHT)
				{
					return;	//超区域了
				}
				break;
			}
		}  	 
    }          
}

//显示字符串
//x,y:起点坐标  
//size:字体大小 
//*p:字符串起始地址 
void OLED_ShowString(uint8_t x,uint8_t y,const uint8_t *p,uint8_t size)
{	
    while((*p<='~')&&(*p>=' '))//判断是不是非法字符!
    {       
        if(x>(128-(size/2)))
		{
			x=0;
			y+=size;
		}
		
        if(y>(64-size))
		{
			y=x=0;
			OLED_Clear();
		}
		
        OLED_ShowChar(x,y,*p,size,1);	
		
        x+=size/2;
        p++;
    }  
	
}

//m^n函数
//返回值:m^n次方.
uint32_t LCD_Pow(uint8_t m,uint8_t n)
{
	uint32_t result=1;	 
	while(n--)result*=m;    
	return result;
}

//显示数字,高位为0,还是显示
//x,y:起点坐标
//num:数值(0~999999999);	 
//len:长度(即要显示的位数)
//size:字体大小
//mode:
//[7]:0,不填充;1,填充0.
//[6:1]:保留
//[0]:0,非叠加显示;1,叠加显示.
void LCD_ShowxNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len,uint8_t size,uint8_t mode)
{  
	uint8_t t,temp;
	uint8_t enshow=0;						   
	for(t=0;t<len;t++)
	{
		temp=(num/LCD_Pow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				if(mode&0X80)OLED_ShowChar(x+(size/2)*t,y,'0',size,mode&0X01);  
				else OLED_ShowChar(x+(size/2)*t,y,' ',size,mode&0X01);  
 				continue;
			}else enshow=1; 
		 	 
		}
	 	OLED_ShowChar(x+(size/2)*t,y,temp+'0',size,mode&0X01); 
	}
} 

//画点 
//x:0~127
//y:0~63
//t:1 填充 0,清空
void OLED_DrawPoint(uint8_t x,uint8_t y,uint8_t t)
{
	uint8_t pos,bx,temp=0;
	if(x>127||y>63)return;//超出范围了. 128*64显示屏
	pos=y/8;
	bx=y%8;
	temp=1<<(bx);
	if(t)
	{
		OLED_GRAM[x][pos]|=temp;
	}
	else 
	{
		OLED_GRAM[x][pos]&=~temp;
	}	    
}






