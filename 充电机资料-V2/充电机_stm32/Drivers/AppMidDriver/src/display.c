#include "global.h"
#include "display.h"
#include "oled_ssd1309.h"
#include "zimo.h"
#include "charge_fsm.h"
#include "stdio.h"
#include "stdlib.h"
#include "app.h"
#include "string.h"
#include "charge_log.h"
#include "tim.h"
#include "battery.h"
#include "FaultCode.h"

uint8_t color_font=1;




void datapros9(void)
{
	 char num[10];

	 /*电池标充次数*/
	 
	 memset(num, 0, 10);
	 sprintf(num,"%5d",(int)(charge_log.batt_charge_timers));
	 display_string_8x16(3,(1+68),(const uint8_t*)(const uint8_t*)&num[0]);
	 display_string_8x16(3,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(3,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(3,(1+104),(const uint8_t*)&num[5]);
	 display_graphic1_12x12(3,(1+110),ci1,color_font);	/*在第1页，第25列显示单个汉字"次"*/	 
	
	 /*	电池快充次数*/
	 memset(num, 0, 10);
//	 sprintf(num,"%5d",(int)(charge_log.batt_fast_charge_timers));
	 display_string_8x16(5,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(5,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(5,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(5,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(5,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(5,(1+104),(const uint8_t*)&num[5]);
	 display_graphic1_12x12(5,(1+110),ci1,color_font);	/*在第1页，第25列显示单个汉字"次"*/	 
	 
	 /*电容充电次数*/
   	 memset(num, 0, 10);
	 sprintf(num,"%5d",(int)(charge_log.cap_charge_timers));
	 display_string_8x16(7,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(7,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(7,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(7,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(7,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(7,(1+104),(const uint8_t*)&num[5]);
	 display_graphic1_12x12(7,(1+110),ci1,color_font);	/*在第1页，第25列显示单个汉字"次"*/	 
}

void dis_cdcs()
{
	
	datapros9();

	display_graphic1_12x12(1,(1+12*2+8),chong1,color_font);	/*在第1页，第25列显示单个汉字"充"*/
	display_graphic1_12x12(1,(1+12*3+8),dian1,color_font); 	/*在第1页，第41列显示单个汉字"电"*/
	display_graphic1_12x12(1,(1+12*4+8),ci1,color_font);	/*在第1页，第49列显示单个汉字"次"*/
	display_graphic1_12x12(1,(1+12*5+8),shu4,color_font); 	/*在第1页，第57列显示单个汉字"数"*/


	display_graphic1_12x12(3,(1+8),dian1,color_font); 	    /*在第2页，第41列显示单个汉字"电"*/
	display_graphic1_12x12(3,(1+12*1+8),chi1,color_font);	/*在第2页，第49列显示单个汉字"池"*/
	display_graphic1_12x12(3,(1+12*2+8),biao1,color_font);  /*在第2页，第57列显示单个汉字"标"*/
	display_graphic1_12x12(3,(1+12*3+8),chong1,color_font); /*在第2页，第65列显示单个汉字"充"*/
	display_graphic1_12x12(3,(1+12*4+8),maohao1,color_font);/*在第3页，第73列显示单个汉字"："*/

	display_graphic1_12x12(5,(1+8),dian1,color_font); 	    /*在第3页，第41列显示单个汉字"电"*/
	display_graphic1_12x12(5,(1+12+8),chi1,color_font);	    /*在第3页，第49列显示单个汉字"池"*/
	display_graphic1_12x12(5,(1+12*2+8),kuai1,color_font);  /*在第3页，第57列显示单个汉字"快"*/
	display_graphic1_12x12(5,(1+12*3+8),chong1,color_font);	/*在第3页，第65列显示单个汉字"充"*/
	display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);/*在第3页，第73列显示单个汉字"："*/

	display_graphic1_12x12(7,(1+8),dian1,color_font); 	    /*在第4页，第41列显示单个汉字"电"*/
	display_graphic1_12x12(7,(1+12+8),rong1,color_font);	/*在第4页，第49列显示单个汉字"容"*/
	display_graphic1_12x12(7,(1+12*2+8),chong1,color_font); /*在第4页，第57列显示单个汉字"充"*/
	display_graphic1_12x12(7,(1+12*3+8),dian1,color_font);	/*在第4页，第65列显示单个汉字"电"*/
	display_graphic1_12x12(7,(1+12*4+8),maohao1,color_font);/*在第4页，第73列显示单个汉字"："*/	
}


 void display_function(uint8_t select_num)	//功能选择函数
{
	uint8_t tmp_sNum = select_num;
	uint8_t tmp_mode = 0x7;
	
	Show_Str(36,0,128,12,"功能选择",12,1);
	
	switch(tmp_sNum)
	{
		case MODE_SELECT_1:
			 tmp_mode = 0x1;
		break;
		case MODE_SELECT_2:
			 tmp_mode = 0x2;
		break;
		case MODE_SELECT_3:
			 tmp_mode = 0x4;		
		break;
		default:
		break;
		
	}
	Show_Str(8,16,128,12,"一、电池充电",12,!((tmp_mode>>0)&0x1));
	Show_Str(8,32,128,12,"二、电池信息",12,!((tmp_mode>>1)&0x1));
	Show_Str(8,48,128,12,"三、作者寄语",12,!((tmp_mode>>2)&0x1));
	OLED_Refresh_Gram();
}

float get_max_battery_voltage_difference(unsigned int *buf, uint8_t buf_len)
{
	unsigned int max_data = *buf;
	unsigned int min_data = *buf;
	unsigned int max_data_difference = 0;
	uint8_t i = 0;
	
	for(i=0;i<buf_len;i++)
	{
		if( *(buf+i) > max_data)
		{
			max_data = *(buf+i);
		}	
	}
	
	for(i=0;i<buf_len;i++)
	{
		if( *(buf+i) < min_data)
		{
			min_data = *(buf+i);
		}	
	}
	
	max_data_difference = max_data - min_data;
	
	return max_data_difference;
}
 
	
/*过程*/
void datapros(void)
{
	 char num[10];
	
	 /* 显示总电压值*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.2f",(float)((g_refresh_device_state.total_battery_voltage) * 0.001));
     display_string_8x16(3,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(3,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(3,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(3,(1+104),(const uint8_t*)&num[5]);
	 display_string_8x16(3,(1+110),"V");
	
	 /*	显示电流值*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.2f",(float)((g_refresh_device_state.battery_current)*0.001));
	 display_string_8x16(5,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(5,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(5,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(5,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(5,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(5,(1+104),(const uint8_t*)&num[5]);
	 display_string_8x16(5,(1+110),"A");
	 
	 /*电池温度*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.1f",(float)((g_refresh_device_state.temp_battery) * 0.5 - 45.0));
	 display_string_8x16(7,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(7,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(7,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(7,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(7,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(7,(1+104),(const uint8_t*)&num[5]);
     display_graphic1_12x12(7,(1+110),Celsius,color_font);	/*在第1页，第25列显示单个汉字"℃"*/	
}

/*完成*/
void datapros1(void)
{
	 float tmp_battery_voltage_difference = get_max_battery_voltage_difference( (unsigned int *)(&(g_refresh_device_state.battery1_voltage)) , 7);
	 char num[10];
	
	 /*显示电压值*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.2f",(float)((g_refresh_device_state.total_battery_voltage) * 0.001));
	 display_string_8x16(3,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(3,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(3,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(3,(1+104),(const uint8_t*)&num[5]);
	 display_string_8x16(3,(1+110),"V");
#if 0
	 /*	显示最大压差*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.2f",(float)( tmp_battery_voltage_difference * 1));
	 display_string_8x16(5,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(5,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(5,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(5,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(5,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(5,(1+108),(const uint8_t*)&num[5]);
	 display_string_8x16(5,(1+110),"mV");
#endif
	 /*电池温度*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.1f",(float)((g_refresh_device_state.temp_battery) * 0.5 - 45.0));
	 display_string_8x16(5,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(5,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(5,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(5,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(5,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(5,(1+104),(const uint8_t*)&num[5]);
	 display_graphic1_12x12(5,(1+110),Celsius,color_font);	/*在第1页，第25列显示单个汉字"℃"*/	
}



void display_dccd(uint8_t mode)	  //电池充电函数 标充 快充
 {
   	color_font=1;
    if(mode == MODE_BATTERY_CHARGE)  //正在充电
    {	 
		
		display_graphic1_12x12(1,(1+12*2+8),zheng1,color_font);	/*在第1页，第25列显示单个汉字"正"*/
		display_graphic1_12x12(1,(1+12*3+8),zai1,color_font); 	/*在第1页，第41列显示单个汉字"在"*/
		display_graphic1_12x12(1,(1+12*4+8),biao1,color_font);	/*在第1页，第49列显示单个汉字"标"*/
		display_graphic1_12x12(1,(1+12*5+8),chong1,color_font); 	/*在第1页，第57列显示单个汉字"充"*/
		//display_string_8x16(1,(1+12*6+8),"...");	/*在第1页，第65列显示字符串"..."*/
		display_graphic1_12x12(1,(1+12*6+8),dunhao1,color_font);
		display_graphic1_12x12(1,(1+12*7+8),dunhao1,color_font); 
		display_graphic1_12x12(1,(1+12*8+8),dunhao1,color_font);

		display_graphic1_12x12(3,(1+8),zong1,color_font); 	/*在第2页，第41列显示单个汉字"总"*/
		display_graphic1_12x12(3,(1+12*1+14),dian1,color_font);	/*在第2页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(3,(1+12*3+8),ya1,color_font);    /*在第2页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(3,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/


		display_graphic1_12x12(5,(1+8),chong1,color_font); 	/*在第3页，第41列显示单个汉字"充"*/
		display_graphic1_12x12(5,(1+12+8),dian1,color_font);	    /*在第3页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(5,(1+12*2+8),dian1,color_font);      /*在第3页，第57列显示单个汉字"电"*/
		display_graphic1_12x12(5,(1+12*3+8),liu1,color_font);	    /*在第3页，第65列显示单个汉字"流"*/
		display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);    /*在第3页，第73列显示单个汉字"："*/

		display_graphic1_12x12(7,(1+8),dian1,color_font); 	    /*在第4页，第41列显示单个汉字"电"*/
		display_graphic1_12x12(7,(1+12+8),chi1,color_font);	    /*在第4页，第49列显示单个汉字"池"*/
		display_graphic1_12x12(7,(1+12*2+8),wen1,color_font);       /*在第4页，第57列显示单个汉字"温"*/
		display_graphic1_12x12(7,(1+12*3+8),du1,color_font);	    /*在第4页，第65列显示单个汉字"度"*/
		display_graphic1_12x12(7,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/		
	}	
    else if(mode == MODE_BATTERY_CHARGE_FINISH)   //充电结束
    {
			 
		display_graphic1_12x12(1,(1+12*2+8),biao1,color_font);	/*在第1页，第25列显示单个汉字"标"*/
		display_graphic1_12x12(1,(1+12*3+8),chong1,color_font); 	/*在第1页，第41列显示单个汉字"充"*/
		display_graphic1_12x12(1,(1+12*4+8),wan1,color_font);	/*在第1页，第49列显示单个汉字"完"*/
		display_graphic1_12x12(1,(1+12*5+8),cheng1,color_font); /*在第1页，第57列显示单个汉字"成"*/
		display_graphic1_12x12(1,(1+12*6+8),tanhao1,color_font);
//		display_string_8x16(1,(1+12*6+8),"!");	    /*在第1页，第65列显示字符串"!"*/
		display_graphic1_12x12(3,(1+8),zong1,color_font); 	/*在第2页，第41列显示单个汉字"总"*/
		display_graphic1_12x12(3,(1+12*1+14),dian1,color_font);	/*在第2页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(3,(1+12*3+8),ya1,color_font);    /*在第2页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(3,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/	

		display_graphic1_12x12(5,(1+8),zui1,color_font); 	/*在第3页，第41列显示单个汉字"最"*/
		display_graphic1_12x12(5,(1+12*1+8),da1,color_font);	/*在第3页，第49列显示单个汉字"大"*/
		display_graphic1_12x12(5,(1+12*2+8),ya1,color_font);    /*在第3页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(5,(1+12*3+8),cha1,color_font);   /*在第3页，第65列显示单个汉字"差"*/
		display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);	    /*在第3页，第73列显示字符串"："*/


		display_graphic1_12x12(7,(1+8),dian1,color_font); 	    /*在第4页，第41列显示单个汉字"电"*/
		display_graphic1_12x12(7,(1+12*1+8),chi1,color_font);	    /*在第4页，第49列显示单个汉字"池"*/
		display_graphic1_12x12(7,(1+12*2+8),wen1,color_font);       /*在第4页，第57列显示单个汉字"温"*/
		display_graphic1_12x12(7,(1+12*3+8),du1,color_font);	    /*在第4页，第65列显示单个汉字"度"*/
		display_graphic1_12x12(7,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/
					
	}
	else if(mode == MODE_BATTERY_FAST_CHARGE)  //快充 正在充电
    {	 
		
		display_graphic1_12x12(1,(1+12*2+8),zheng1,color_font);	/*在第1页，第25列显示单个汉字"正"*/
		display_graphic1_12x12(1,(1+12*3+8),zai1,color_font); 	/*在第1页，第41列显示单个汉字"在"*/
		display_graphic1_12x12(1,(1+12*4+8),kuai1,color_font);	/*在第1页，第49列显示单个汉字"快"*/
		display_graphic1_12x12(1,(1+12*5+8),chong1,color_font); 	/*在第1页，第57列显示单个汉字"充"*/
		//display_string_8x16(1,(1+12*6+8),"...");	/*在第1页，第65列显示字符串"..."*/
		display_graphic1_12x12(1,(1+12*6+8),dunhao1,color_font);
		display_graphic1_12x12(1,(1+12*7+8),dunhao1,color_font); 
		display_graphic1_12x12(1,(1+12*8+8),dunhao1,color_font);

		display_graphic1_12x12(3,(1+8),zong1,color_font); 	/*在第2页，第41列显示单个汉字"总"*/
		display_graphic1_12x12(3,(1+12*1+14),dian1,color_font);	/*在第2页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(3,(1+12*3+8),ya1,color_font);    /*在第2页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(3,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/


		display_graphic1_12x12(5,(1+8),chong1,color_font); 	/*在第3页，第41列显示单个汉字"充"*/
		display_graphic1_12x12(5,(1+12+8),dian1,color_font);	    /*在第3页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(5,(1+12*2+8),dian1,color_font);      /*在第3页，第57列显示单个汉字"电"*/
		display_graphic1_12x12(5,(1+12*3+8),liu1,color_font);	    /*在第3页，第65列显示单个汉字"流"*/
		display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);    /*在第3页，第73列显示单个汉字"："*/

		display_graphic1_12x12(7,(1+8),dian1,color_font); 	    /*在第4页，第41列显示单个汉字"电"*/
		display_graphic1_12x12(7,(1+12+8),chi1,color_font);	    /*在第4页，第49列显示单个汉字"池"*/
		display_graphic1_12x12(7,(1+12*2+8),wen1,color_font);       /*在第4页，第57列显示单个汉字"温"*/
		display_graphic1_12x12(7,(1+12*3+8),du1,color_font);	    /*在第4页，第65列显示单个汉字"度"*/
		display_graphic1_12x12(7,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/		
	}	
    else if(mode == MODE_BATTERY_FAST_CHARGE_FINISH)   //充电结束
    {
			 
		display_graphic1_12x12(1,(1+12*2+8),kuai1,color_font);	/*在第1页，第25列显示单个汉字"快"*/
		display_graphic1_12x12(1,(1+12*3+8),chong1,color_font); 	/*在第1页，第41列显示单个汉字"充"*/
		display_graphic1_12x12(1,(1+12*4+8),wan1,color_font);	/*在第1页，第49列显示单个汉字"完"*/
		display_graphic1_12x12(1,(1+12*5+8),cheng1,color_font); /*在第1页，第57列显示单个汉字"成"*/
		display_graphic1_12x12(1,(1+12*6+8),tanhao1,color_font);
//		display_string_8x16(1,(1+12*6+8),"!");	    /*在第1页，第65列显示字符串"!"*/
		display_graphic1_12x12(3,(1+8),zong1,color_font); 	/*在第2页，第41列显示单个汉字"总"*/
		display_graphic1_12x12(3,(1+12*1+14),dian1,color_font);	/*在第2页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(3,(1+12*3+8),ya1,color_font);    /*在第2页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(3,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/	

		display_graphic1_12x12(5,(1+8),zui1,color_font); 	/*在第3页，第41列显示单个汉字"最"*/
		display_graphic1_12x12(5,(1+12*1+8),da1,color_font);	/*在第3页，第49列显示单个汉字"大"*/
		display_graphic1_12x12(5,(1+12*2+8),ya1,color_font);    /*在第3页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(5,(1+12*3+8),cha1,color_font);   /*在第3页，第65列显示单个汉字"差"*/
		display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);	    /*在第3页，第73列显示字符串"："*/


		display_graphic1_12x12(7,(1+8),dian1,color_font); 	    /*在第4页，第41列显示单个汉字"电"*/
		display_graphic1_12x12(7,(1+12*1+8),chi1,color_font);	    /*在第4页，第49列显示单个汉字"池"*/
		display_graphic1_12x12(7,(1+12*2+8),wen1,color_font);       /*在第4页，第57列显示单个汉字"温"*/
		display_graphic1_12x12(7,(1+12*3+8),du1,color_font);	    /*在第4页，第65列显示单个汉字"度"*/
		display_graphic1_12x12(7,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/
					
	}
	else
	{
		
	}
 
 }

void display_dcwh(uint8_t mode)	  
 {
   	color_font=1;
	 
    if(mode == MODE_BATTERY_FIX)  //正在维护
    {	 

		display_graphic1_12x12(1,(1+12*2+8),zheng1,color_font);	/*在第1页，第25列显示单个汉字"正"*/
		display_graphic1_12x12(1,(1+12*3+8),zai1,color_font); 	/*在第1页，第41列显示单个汉字"在"*/
		display_graphic1_12x12(1,(1+12*4+8),wei1,color_font);	/*在第1页，第49列显示单个汉字"维"*/
		display_graphic1_12x12(1,(1+12*5+8),hu1,color_font); 	/*在第1页，第57列显示单个汉字"护"*/
		//display_string_8x16(1,(1+12*6+8),"...");	/*在第1页，第65列显示字符串"..."*/	
		display_graphic1_12x12(1,(1+12*6+8),dunhao1,color_font);
		display_graphic1_12x12(1,(1+12*7+8),dunhao1,color_font); 
		display_graphic1_12x12(1,(1+12*8+8),dunhao1,color_font);

		display_graphic1_12x12(3,(1+8),zong1,color_font); 	/*在第2页，第41列显示单个汉字"总"*/
		display_graphic1_12x12(3,(1+12*1+14),dian1,color_font);	/*在第2页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(3,(1+12*3+8),ya1,color_font);    /*在第2页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(3,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/

		display_graphic1_12x12(5,(1+8),dian1,color_font);	    /*在第3页，第41列显示单个汉字"电"*/
		display_graphic1_12x12(5,(1+12*3+8),liu1,color_font);       /*在第3页，第49列显示单个汉字"流"*/
		display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);    /*在第3页，第57列显示单个汉字"冒号"*/

		display_graphic1_12x12(7,(1+8),dian1,color_font); 	    /*在第4页，第41列显示单个汉字"电"*/
		display_graphic1_12x12(7,(1+12*1+8),chi1,color_font);	    /*在第4页，第49列显示单个汉字"池"*/
		display_graphic1_12x12(7,(1+12*2+8),wen1,color_font);       /*在第4页，第57列显示单个汉字"温"*/
		display_graphic1_12x12(7,(1+12*3+8),du1,color_font);	    /*在第4页，第65列显示单个汉字"度"*/
		display_graphic1_12x12(7,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/

			
			
	}	
    else if(mode == MODE_BATTERY_FIX_FINISH)   //维护完成
    {
			
		display_graphic1_12x12(1,(1+12*2+8),wei1,color_font);	/*在第1页，第25列显示单个汉字"维"*/
		display_graphic1_12x12(1,(1+12*3+8),hu1,color_font); 	/*在第1页，第41列显示单个汉字"护"*/
		display_graphic1_12x12(1,(1+12*4+8),wan1,color_font);	/*在第1页，第49列显示单个汉字"完"*/
		display_graphic1_12x12(1,(1+12*5+8),cheng1,color_font); /*在第1页，第57列显示单个汉字"成"*/
//		display_string_8x16(1,(1+12*6+8),"!");	    /*在第1页，第65列显示字符串"!"*/
		display_graphic1_12x12(1,(1+12*6+8),tanhao1,color_font);
		
		display_graphic1_12x12(3,(1+8),zong1,color_font); 	/*在第2页，第41列显示单个汉字"总"*/
		display_graphic1_12x12(3,(1+12*1+8),dian1,color_font);	/*在第2页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(3,(1+12*2+8),ya1,color_font);    /*在第2页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(3,(1+12*3+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/	

		display_graphic1_12x12(5,(1+8),zui1,color_font); 	/*在第3页，第41列显示单个汉字"最"*/
		display_graphic1_12x12(5,(1+12*1+8),da1,color_font);	/*在第3页，第49列显示单个汉字"大"*/
		display_graphic1_12x12(5,(1+12*2+8),ya1,color_font);    /*在第3页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(5,(1+12*3+8),cha1,color_font);   /*在第3页，第65列显示单个汉字"差"*/
		display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/	

		display_graphic1_12x12(7,(1+8),dian1,color_font); 	    /*在第4页，第41列显示单个汉字"电"*/
		display_graphic1_12x12(7,(1+12*1+8),chi1,color_font);	    /*在第4页，第49列显示单个汉字"池"*/
		display_graphic1_12x12(7,(1+12*2+8),wen1,color_font);       /*在第4页，第57列显示单个汉字"温"*/
		display_graphic1_12x12(7,(1+12*3+8),du1,color_font);	    /*在第4页，第65列显示单个汉字"度"*/
		display_graphic1_12x12(7,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/
					
		}	
    
	}
 
void datapros4(void)
{
	char num[10];
	/*当前电压：*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.2f",(float)((g_refresh_device_state.capacitor_voltage) * 0.001));
     display_string_8x16(3,(1+12*5+8),(const uint8_t*)&num[0]);
	 display_string_8x16(3,(1+12*5+16),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(1+12*5+24),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(1+12*5+32),(const uint8_t*)&num[3]);
	 display_string_8x16(3,(1+12*5+40),(const uint8_t*)&num[4]);
	 display_string_8x16(3,(1+12*5+48),(const uint8_t*)&num[5]);
	
     display_string_8x16(3,(1+12*5+56),"V");
	 
	 /*充电电流：*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.2f",(float)((g_refresh_device_state.capacitor_current) * 0.001));
	 display_string_8x16(5,(1+12*5+8),(const uint8_t*)&num[0]);
	 display_string_8x16(5,(1+12*5+16),(const uint8_t*)&num[1]);
	 display_string_8x16(5,(1+12*5+24),(const uint8_t*)&num[2]);
	 display_string_8x16(5,(1+12*5+32),(const uint8_t*)&num[3]);
	 display_string_8x16(5,(1+12*5+40),(const uint8_t*)&num[4]);
	 display_string_8x16(5,(1+12*5+48),(const uint8_t*)&num[5]);
	
     display_string_8x16(5,(1+12*5+56),"A");
}

void datapros6(void)
{
	char num[10];
	
	/*电容电压：*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.2f",(float)((g_refresh_device_state.capacitor_voltage) * 0.001));
     display_string_8x16(5,(1+12*5+8),(const uint8_t*)&num[0]);
	 display_string_8x16(5,(1+12*5+16),(const uint8_t*)&num[1]);
	 display_string_8x16(5,(1+12*5+24),(const uint8_t*)&num[2]);
	 display_string_8x16(5,(1+12*5+32),(const uint8_t*)&num[3]);
	 display_string_8x16(5,(1+12*5+40),(const uint8_t*)&num[4]);
	 display_string_8x16(5,(1+12*5+48),(const uint8_t*)&num[5]);
	
     display_string_8x16(5,(1+12*5+56),"V");
	 
}

/*电容正在充电*/
void display_drcd(uint8_t mode)
{
	if(MODE_CAPACITANCE_CHARGE == mode)
	{
		color_font=1;
	    display_graphic1_12x12(1,(1+12*2+8),zheng1,color_font);	/*在第1页，第25列显示单个汉字"正"*/
		display_graphic1_12x12(1,(1+12*3+8),zai1,color_font); 	/*在第1页，第41列显示单个汉字"在"*/
		display_graphic1_12x12(1,(1+12*4+8),chong1,color_font);	/*在第1页，第49列显示单个汉字"充"*/
		display_graphic1_12x12(1,(1+12*5+8),dian1,color_font); 	/*在第1页，第57列显示单个汉字"点"*/
		//display_string_8x16(1,(1+12*6+8),"...");	/*在第1页，第65列显示字符串"..."*/
		display_graphic1_12x12(1,(1+12*6+8),dunhao1,color_font);
		display_graphic1_12x12(1,(1+12*7+8),dunhao1,color_font); 
		display_graphic1_12x12(1,(1+12*8+8),dunhao1,color_font);

		display_graphic1_12x12(3,(1+8),dang1,color_font); 	/*在第2页，第41列显示单个汉字"当"*/
		display_graphic1_12x12(3,(1+12*1+8),qian1,color_font);	/*在第2页，第49列显示单个汉字"前"*/
		display_graphic1_12x12(3,(1+12*2+8),dian1,color_font);	/*在第2页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(3,(1+12*3+8),ya1,color_font);    /*在第2页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(3,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/


		display_graphic1_12x12(5,(1+8),chong1,color_font); 	/*在第3页，第41列显示单个汉字"充"*/
		display_graphic1_12x12(5,(1+12+8),dian1,color_font);	    /*在第3页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(5,(1+12*2+8),dian1,color_font);      /*在第3页，第57列显示单个汉字"电"*/
		display_graphic1_12x12(5,(1+12*3+8),liu1,color_font);	    /*在第3页，第65列显示单个汉字"流"*/
		display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);    /*在第3页，第73列显示单个汉字"："*/
	}
	else if(MODE_CAPACITANCE_CHARGE_FINISH == mode)   //电容充电完成
	{
		color_font=1;
	    display_graphic1_12x12(1,(1+12*2+8),chong1,color_font);	/*在第1页，第25列显示单个汉字"充"*/
		display_graphic1_12x12(1,(1+12*3+8),dian1,color_font); 	/*在第1页，第41列显示单个汉字"电"*/
		display_graphic1_12x12(1,(1+12*4+8),wan1,color_font);	/*在第1页，第49列显示单个汉字"完"*/
		display_graphic1_12x12(1,(1+12*5+8),cheng1,color_font); 	/*在第1页，第57列显示单个汉字"成"*/
		//display_string_8x16(1,(1+12*6+8),"!");	/*在第1页，第65列显示字符串"！"*/
		display_graphic1_12x12(1,(1+12*6+8),tanhao1,color_font);

		display_graphic1_12x12(5,(1+8),dian1,color_font); 	/*在第2页，第41列显示单个汉字"电"*/
		display_graphic1_12x12(5,(1+12*1+8),rong1,color_font);	/*在第2页，第49列显示单个汉字"容"*/
		display_graphic1_12x12(5,(1+12*2+8),dian1,color_font);	/*在第2页，第49列显示单个汉字"电"*/
		display_graphic1_12x12(5,(1+12*3+8),ya1,color_font);    /*在第2页，第57列显示单个汉字"压"*/
		display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/

	}
}

/*电池信息显示*/
void datapros5(void)
{
	 float tmp_battery_voltage_difference = get_max_battery_voltage_difference( (unsigned int *)(&(g_refresh_device_state.battery1_voltage)) , 7);
	 char num[10];
	 

	 /*显示电压值*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.2f",(float)((g_refresh_device_state.total_battery_voltage) * 0.001));
	 display_string_8x16(1,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(1,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(1,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(1,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(1,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(1,(1+104),(const uint8_t*)&num[5]);
	 display_string_8x16(1,(1+110),"V");
	 /*	显示最大压差*/
#if 0
	 memset(num, 0, 10);
	 sprintf(num,"%3.1f",(float)( tmp_battery_voltage_difference * 1));
	 display_string_8x16(3,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(3,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(1+92),(const uint8_t*)&num[3]);
//	 display_string_8x16(3,(1+100),(const uint8_t*)&num[4]);
//	 display_string_8x16(3,(1+108),(const uint8_t*)&num[5]);
	 display_string_8x16(3,(1+110),"mV");
#endif
	 /*电池容量*/
	 memset(num, 0, 10);
	 sprintf(num,"%5d",(int)(g_battery_per.batt_per));
	 display_string_8x16(3,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(3,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(3,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(3,(1+104),(const uint8_t*)&num[5]);

	 display_string_8x16(3,(1+110),"%");
	 
	 /*充电次数*/ //标充的充电次数
	 memset(num, 0, 10);
	 sprintf(num,"%5d",(int)(charge_log.batt_charge_timers));
	 display_string_8x16(5,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(5,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(5,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(5,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(5,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(5,(1+104),(const uint8_t*)&num[5]);
	 display_graphic1_12x12(5,(1+110),ci1,color_font);	/*在第1页，第25列显示单个汉字"℃"*/	 
	 
	 /*充电次数*/ //快充的充电次数
	 memset(num, 0, 10);
//	 sprintf(num,"%5d",(int)(charge_log.batt_fast_charge_timers));
	 display_string_8x16(7,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(7,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(7,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(7,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(7,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(7,(1+104),(const uint8_t*)&num[5]);
	 display_graphic1_12x12(7,(1+110),ci1,color_font);	/*在第1页，第25列显示单个汉字"℃"*/	 
}
void datapros7(void)
{
	 char num[10];
	

	 /*显示电压值*/
	 memset(num, 0, 10);
	 sprintf(num,"%5.2f",(float)((g_refresh_device_state.total_battery_voltage) * 0.001));
	 display_string_8x16(2,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(2,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(2,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(2,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(2,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(2,(1+104),(const uint8_t*)&num[5]);
	 display_string_8x16(2,(1+110),"V");


	 
	 /*电容充电次数*/
	 memset(num, 0, 10);
	 sprintf(num,"%5d",(int)(charge_log.cap_charge_timers));
	 display_string_8x16(6,(1+68),(const uint8_t*)&num[0]);
	 display_string_8x16(6,(1+76),(const uint8_t*)&num[1]);
	 display_string_8x16(6,(1+84),(const uint8_t*)&num[2]);
	 display_string_8x16(6,(1+92),(const uint8_t*)&num[3]);
	 display_string_8x16(6,(1+100),(const uint8_t*)&num[4]);
	 display_string_8x16(6,(1+104),(const uint8_t*)&num[5]);
	 display_graphic1_12x12(6,(1+110),ci1,color_font);	/*在第1页，第25列显示单个汉字"℃"*/	 
}
		
 void display_fzxx(uint8_t mode)	 //查看负载信息
	{
	    color_font=1;
		if(mode == MODE_BATTERY_INFO)
		{			
				display_graphic1_12x12(1,(1+8),dian1,color_font);	/*在第1页，第25列显示单个汉字"电"*/
			    display_graphic1_12x12(1,(1+12*1+8),chi1,color_font); 	/*在第1页，第41列显示单个汉字"池"*/
				display_graphic1_12x12(1,(1+12*2+8),dian1,color_font); 	/*在第1页，第41列显示单个汉字"电"*/
				display_graphic1_12x12(1,(1+12*3+8),ya1,color_font);	/*在第1页，第49列显示单个汉字"压"*/
				display_graphic1_12x12(1,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/
		
//				display_graphic1_12x12(3,(1+8),zui1,color_font); 	/*在第3页，第41列显示单个汉字"最"*/
//				display_graphic1_12x12(3,(1+12*1+8),da1,color_font);	/*在第3页，第49列显示单个汉字"大"*/
//				display_graphic1_12x12(3,(1+12*2+8),ya1,color_font);    /*在第3页，第57列显示单个汉字"压"*/
//				display_graphic1_12x12(3,(1+12*3+8),cha1,color_font);   /*在第3页，第65列显示单个汉字"差"*/
//				display_graphic1_12x12(3,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/
		
		
				display_graphic1_12x12(3,(1+8),dian1,color_font); 	/*在第3页，第41列显示单个汉字"电"*/
				display_graphic1_12x12(3,(1+12*1+8),chi1,color_font);	/*在第3页，第49列显示单个汉字"池"*/
				display_graphic1_12x12(3,(1+12*2+8),rong1,color_font);    /*在第3页，第57列显示单个汉字"容"*/
				display_graphic1_12x12(3,(1+12*3+8),liang1,color_font);   /*在第3页，第65列显示单个汉字"量"*/
				display_graphic1_12x12(3,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/
		
				display_graphic1_12x12(5,(1+8),biao1,color_font); 	    /*在第4页，第41列显示单个汉字"充"*/
				display_graphic1_12x12(5,(1+12*1+8),chong1,color_font);	    /*在第4页，第49列显示单个汉字"电"*/
				display_graphic1_12x12(5,(1+12*2+8),ci1,color_font);       /*在第4页，第57列显示单个汉字"次"*/
				display_graphic1_12x12(5,(1+12*3+8),shu4,color_font);	    /*在第4页，第65列显示单个汉字"数"*/
				display_graphic1_12x12(5,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/
				
				display_graphic1_12x12(7,(1+8),kuai1,color_font); 	    /*在第4页，第41列显示单个汉字"充"*/
				display_graphic1_12x12(7,(1+12*1+8),chong1,color_font);	    /*在第4页，第49列显示单个汉字"电"*/
				display_graphic1_12x12(7,(1+12*2+8),ci1,color_font);       /*在第4页，第57列显示单个汉字"次"*/
				display_graphic1_12x12(7,(1+12*3+8),shu4,color_font);	    /*在第4页，第65列显示单个汉字"数"*/
				display_graphic1_12x12(7,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/
		}
		else if(mode == MODE_CAPACITANCE_INFO)
		{		
				display_graphic1_12x12(2,(1+8),dian1,color_font);	/*在第1页，第25列显示单个汉字"电"*/
			    display_graphic1_12x12(2,(1+12*1+8),rong1,color_font); 	/*在第1页，第41列显示单个汉字"容"*/
				display_graphic1_12x12(2,(1+12*2+8),dian1,color_font); 	/*在第1页，第41列显示单个汉字"电"*/
				display_graphic1_12x12(2,(1+12*3+8),ya1,color_font);	/*在第1页，第49列显示单个汉字"压"*/
				display_graphic1_12x12(2,(1+12*4+8),maohao1,color_font);/*在第2页，第65列显示单个汉字"："*/

#if 0			
				display_graphic1_12x12(4,(1+8),dian1,color_font); 	/*在第3页，第41列显示单个汉字"电"*/
				display_graphic1_12x12(4,(1+12*1+8),rong1,color_font);	/*在第3页，第49列显示单个汉字"容"*/
				display_graphic1_12x12(4,(1+12*2+8),rong1,color_font);    /*在第3页，第57列显示单个汉字"容"*/
				display_graphic1_12x12(4,(1+12*3+8),liang1,color_font);   /*在第3页，第65列显示单个汉字"量"*/
				display_graphic1_12x12(4,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/
#endif
		
				display_graphic1_12x12(6,(1+8),chong1,color_font); 	    /*在第4页，第41列显示单个汉字"充"*/
				display_graphic1_12x12(6,(1+12*1+8),dian1,color_font);	    /*在第4页，第49列显示单个汉字"电"*/
				display_graphic1_12x12(6,(1+12*2+8),ci1,color_font);       /*在第4页，第57列显示单个汉字"次"*/
				display_graphic1_12x12(6,(1+12*3+8),shu4,color_font);	    /*在第4页，第65列显示单个汉字"数"*/
				display_graphic1_12x12(6,(1+12*4+8),maohao1,color_font);    /*在第4页，第73列显示单个汉字"："*/
		}
		else if(mode == MODE_MISSED_LOAD)
		{
				display_graphic1_12x12(4,(1+12*3+8),wei2,color_font); 	/*在第3页，第41列显示单个汉字"未"*/
				display_graphic1_12x12(4,(1+12*4+8),jie1,color_font);	/*在第3页，第49列显示单个汉字"接"*/
				display_graphic1_12x12(4,(1+12*5+8),fu1,color_font);    /*在第3页，第57列显示单个汉字"负"*/
				display_graphic1_12x12(4,(1+12*6+8),zai2,color_font);   /*在第3页，第65列显示单个汉字"载"*/				
		}

	}

void datapros3(void)
{
	 char num[10];
#if 0	
	 /*C1*/
	 memset(num, 0, 10);
	 sprintf(num,"%3.1f",(float)((g_refresh_device_state.battery1_voltage) * 0.001));
	 display_string_8x16(1,(25),(const uint8_t*)&num[0]);
	 display_string_8x16(1,(25+8),(const uint8_t*)&num[1]);
	 display_string_8x16(1,(25+16),(const uint8_t*)&num[2]);
	 display_string_8x16(1,(25+24),(const uint8_t*)&num[3]);

	/*C2*/
	 memset(num, 0, 10);
	 sprintf(num,"%3.1f",(float)((g_refresh_device_state.battery2_voltage) * 0.001));
	 display_string_8x16(1,(88),(const uint8_t*)&num[0]);
	 display_string_8x16(1,(88+8),(const uint8_t*)&num[1]);
	 display_string_8x16(1,(88+16),(const uint8_t*)&num[2]);
	 display_string_8x16(1,(88+24),(const uint8_t*)&num[3]);


	 /*C3*/
	 memset(num, 0, 10);
	 sprintf(num,"%3.1f",(float)((g_refresh_device_state.battery3_voltage) * 0.001));
	 display_string_8x16(3,(25),(const uint8_t*)&num[0]);
	 display_string_8x16(3,(25+8),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(25+16),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(25+24),(const uint8_t*)&num[3]);

	/*C4*/
	 memset(num, 0, 10);
	 sprintf(num,"%3.1f",(float)((g_refresh_device_state.battery4_voltage) * 0.001));
	 display_string_8x16(3,(88),(const uint8_t*)&num[0]);
	 display_string_8x16(3,(88+8),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(88+16),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(88+24),(const uint8_t*)&num[3]);
#else
	 /*C1*/
	 memset(num, 0, 10);
	 sprintf(num,"%04X",((g_refresh_device_state.battery1_voltage)));
	 display_string_8x16(1,(25),(const uint8_t*)&num[0]);
	 display_string_8x16(1,(25+8),(const uint8_t*)&num[1]);
	 display_string_8x16(1,(25+16),(const uint8_t*)&num[2]);
	 display_string_8x16(1,(25+24),(const uint8_t*)&num[3]);

	/*C2*/
	 memset(num, 0, 10);
	 sprintf(num,"%04X",((g_refresh_device_state.battery2_voltage)));
	 display_string_8x16(1,(88),(const uint8_t*)&num[0]);
	 display_string_8x16(1,(88+8),(const uint8_t*)&num[1]);
	 display_string_8x16(1,(88+16),(const uint8_t*)&num[2]);
	 display_string_8x16(1,(88+24),(const uint8_t*)&num[3]);


	 /*C3*/
	 memset(num, 0, 10);
	 sprintf(num,"%04X",((g_refresh_device_state.battery3_voltage)));
	 display_string_8x16(3,(25),(const uint8_t*)&num[0]);
	 display_string_8x16(3,(25+8),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(25+16),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(25+24),(const uint8_t*)&num[3]);

	/*C4*/
	 memset(num, 0, 10);
	 sprintf(num,"%04X",((g_refresh_device_state.battery4_voltage)));
	 display_string_8x16(3,(88),(const uint8_t*)&num[0]);
	 display_string_8x16(3,(88+8),(const uint8_t*)&num[1]);
	 display_string_8x16(3,(88+16),(const uint8_t*)&num[2]);
	 display_string_8x16(3,(88+24),(const uint8_t*)&num[3]);
#endif
	 /*C5*/
	 memset(num, 0, 10);
	 sprintf(num,"%3.1f",(float)((g_refresh_device_state.battery5_voltage) * 0.001));
	 display_string_8x16(5,(25),(const uint8_t*)&num[0]);
	 display_string_8x16(5,(25+8),(const uint8_t*)&num[1]);
	 display_string_8x16(5,(25+16),(const uint8_t*)&num[2]);
	 display_string_8x16(5,(25+24),(const uint8_t*)&num[3]);

	/*C6*/
	 memset(num, 0, 10);
	 sprintf(num,"%3.1f",(float)((g_refresh_device_state.battery6_voltage) * 0.001));
	 display_string_8x16(5,(88),(const uint8_t*)&num[0]);
	 display_string_8x16(5,(88+8),(const uint8_t*)&num[1]);
	 display_string_8x16(5,(88+16),(const uint8_t*)&num[2]);
	 display_string_8x16(5,(88+24),(const uint8_t*)&num[3]);
	 
	  /*C7*/
	 memset(num, 0, 10);
	 sprintf(num,"%3.1f",(float)((g_refresh_device_state.battery7_voltage) * 0.001));
	 display_string_8x16(7,(25),(const uint8_t*)&num[0]);
	 display_string_8x16(7,(25+8),(const uint8_t*)&num[1]);
	 display_string_8x16(7,(25+16),(const uint8_t*)&num[2]);
	 display_string_8x16(7,(25+24),(const uint8_t*)&num[3]);
	 
	  /*T*/
	 memset(num, 0, 10);
	 sprintf(num,"%3.1f",(float)((g_refresh_device_state.temp_battery) * 0.5 - 45.0));
	 display_string_8x16(7,(88),(const uint8_t*)&num[0]);
	 display_string_8x16(7,(88+8),(const uint8_t*)&num[1]);
	 display_string_8x16(7,(88+16),(const uint8_t*)&num[2]);
	 display_string_8x16(7,(88+24),(const uint8_t*)&num[3]);
	 display_string_8x16(7,(88+32),(const uint8_t*)&num[4]);
	 display_string_8x16(7,(88+40),(const uint8_t*)&num[5]);

}

void display_dtxx()    //单体电压及电池温度显示界面
{
	display_string_8x16(1,(1),"C1:");
	display_string_8x16(1,(64),"C2:");
	display_string_8x16(3,(1),"C3:");
	display_string_8x16(3,(64),"C4:");
	display_string_8x16(5,(1),"C5:");
	display_string_8x16(5,(64),"C6:");
	display_string_8x16(7,(1),"C7:");
	display_string_8x16(7,(64),"T:");
}

void datapros8(void)
{
	 char num[10];
	
	 /* CHARGE_HARD_VER		1.0 */
	 memset(num, 0, 10);
	 sprintf(num,"%3.2f",(float)(CHARGE_HARD_VER));
	 display_string_8x16(2,(1+12*7),(const uint8_t*)&num[0]);
	 display_string_8x16(2,(1+12*7+8),(const uint8_t*)&num[1]);
	 display_string_8x16(2,(1+12*7+16),(const uint8_t*)&num[2]);
	 display_string_8x16(2,(1+12*7+24),(const uint8_t*)&num[3]);
	 display_string_8x16(2,(1+12*7+32),(const uint8_t*)&num[4]);


	/* CHARGE_SOFT_VER		1.0*/
	 memset(num, 0, 10);
	 sprintf(num,"%3.2f",(float)(CHARGE_SOFT_VER));
	 display_string_8x16(4,(1+12*7),(const uint8_t*)&num[0]);
	 display_string_8x16(4,(1+12*7+8),(const uint8_t*)&num[1]);
	 display_string_8x16(4,(1+12*7+16),(const uint8_t*)&num[2]);
	 display_string_8x16(4,(1+12*7+24),(const uint8_t*)&num[3]);
	 display_string_8x16(4,(1+12*7+32),(const uint8_t*)&num[4]);
}

void display_bbxh() //版本型号界面
{
	color_font = 1;
	
	datapros8();
	display_graphic1_12x12(2,(1+8),ying2,color_font);	/*在第1页，第25列显示单个汉字"硬"*/
	display_graphic1_12x12(2,(1+12*1+8),jian1,color_font); 	/*在第1页，第41列显示单个汉字"件"*/
	display_graphic1_12x12(2,(1+12*2+8),ban1,color_font);	/*在第1页，第49列显示单个汉字"版"*/
	display_graphic1_12x12(2,(1+12*3+8),ben1,color_font); /*在第1页，第57列显示单个汉字"本"*/
	display_graphic1_12x12(2,(1+12*4+8),hao1,color_font); /*在第1页，第57列显示单个汉字"号"*/
	display_graphic1_12x12(2,(1+12*5+8),maohao1,color_font); /*在第1页，第57列显示单个汉字"："*/
	
	


	display_graphic1_12x12(4,(1+8),ruan1,color_font);	/*在第1页，第25列显示单个汉字"软"*/
	display_graphic1_12x12(4,(1+12*1+8),jian1,color_font); 	/*在第1页，第41列显示单个汉字"件"*/
	display_graphic1_12x12(4,(1+12*2+8),ban1,color_font);	/*在第1页，第49列显示单个汉字"版"*/
	display_graphic1_12x12(4,(1+12*3+8),ben1,color_font); /*在第1页，第57列显示单个汉字"本"*/
	display_graphic1_12x12(4,(1+12*4+8),hao1,color_font); /*在第1页，第57列显示单个汉字"号"*/
	display_graphic1_12x12(4,(1+12*5+8),maohao1,color_font); /*在第1页，第57列显示单个汉字"："*/
}

void display_gzxs(uint8_t mode)  //故障显示函数
{
	  
	display_graphic1_12x12(1,(1+12*2+8),jian2,color_font);	/*在第1页，第25列显示单个汉字"检"*/
	display_graphic1_12x12(1,(1+12*3+8),ce1,color_font); 	/*在第1页，第41列显示单个汉字"测"*/
	display_graphic1_12x12(1,(1+12*4+8),dao1,color_font);	/*在第1页，第49列显示单个汉字"到"*/
	display_graphic1_12x12(1,(1+12*5+8),gu1,color_font); 	/*在第1页，第41列显示单个汉字"故"*/
	display_graphic1_12x12(1,(1+12*6+8),zhang1,color_font); /*在第1页，第57列显示单个汉字"障"*/
	//display_string_8x16(1,(1+12*7+8),"!"); /*在第1页，第57列显示单个汉字"："*/
	display_graphic1_12x12(1,(1+12*7+8),tanhao1,color_font);

	if(MODE_ERR_COMM == mode)
	{	
		display_graphic1_12x12(4,(1+12*1+8),gu1,color_font);	/*在第3页，第25列显示单个汉字"故"*/
		display_graphic1_12x12(4,(1+12*2+8),zhang1,color_font); /*在第3页，第41列显示单个汉字"障"*/
		display_graphic1_12x12(4,(1+12*3+8),lei1,color_font);	/*在第3页，第49列显示单个汉字"类"*/
		display_graphic1_12x12(4,(1+12*4+8),x1,color_font);  /*在第3页，第57列显示单个汉字"型"*/
		display_graphic1_12x12(4,(1+12*5+8),maohao1,color_font);/*在第3页，第65列显示单个汉字"："*/  
		
		display_graphic1_12x12(4,(1+12*6+8),tong1,color_font);	/*在第6页，第73列显示单个汉字"通"*/
		display_graphic1_12x12(4,(1+12*7+8),xun1,color_font); 	/*在第6页，第81列显示单个汉字"讯"*/
		display_graphic1_12x12(4,(1+12*8+8),zhong1,color_font);	/*在第6页，第89列显示单个汉字"中"*/
		display_graphic1_12x12(4,(1+12*9+8),duan1,color_font); 	/*在第6页，第98列显示单个汉字"断"*/	
	}
	else if(MODE_FAULT_CODE == mode)
	{  
		faultCode_task();
	}
	else
	{
		
	}
	


	
}

//==========================================================
/*按优先级，故障显示界面*/
void fault_disp(uint8_t line_num,uint32_t mode)
{
	uint8_t disp_data[WORDS_NUM][STRING_NUM] = {0}; //一行7个字，一个字24个字节
	uint8_t i = 0;
		
	switch(mode)
	{
		case 1: //输出短路
			memcpy(disp_data[0],shu1,STRING_NUM);
			memcpy(disp_data[1],chu1,STRING_NUM);
			memcpy(disp_data[2],uan1,STRING_NUM);
			memcpy(disp_data[3],lu1,STRING_NUM);
			memcpy(disp_data[4],heiping,STRING_NUM);
			memcpy(disp_data[5],heiping,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 2://电池组连接异常
			memcpy(disp_data[0],dian1,STRING_NUM);
			memcpy(disp_data[1],chi1,STRING_NUM);
			memcpy(disp_data[2],zu1,STRING_NUM);
			memcpy(disp_data[3],lian1,STRING_NUM);
			memcpy(disp_data[4],jie1,STRING_NUM);
			memcpy(disp_data[5],yi1,STRING_NUM);
			memcpy(disp_data[6],chang1,STRING_NUM);
			break;
		case 3://单体短路
			memcpy(disp_data[0],dan1,STRING_NUM);
			memcpy(disp_data[1],ti1,STRING_NUM);
			memcpy(disp_data[2],uan1,STRING_NUM);
			memcpy(disp_data[3],lu1,STRING_NUM);
			memcpy(disp_data[4],heiping,STRING_NUM);
			memcpy(disp_data[5],heiping,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 4://单体断路
			memcpy(disp_data[0],dan1,STRING_NUM);
			memcpy(disp_data[1],ti1,STRING_NUM);
			memcpy(disp_data[2],duan1,STRING_NUM);
			memcpy(disp_data[3],lu1,STRING_NUM);
			memcpy(disp_data[4],heiping,STRING_NUM);
			memcpy(disp_data[5],heiping,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 5://充电电流过大
			memcpy(disp_data[0],chong1,STRING_NUM);
			memcpy(disp_data[1],dian1,STRING_NUM);
			memcpy(disp_data[2],dian1,STRING_NUM);
			memcpy(disp_data[3],liu1,STRING_NUM);
			memcpy(disp_data[4],guo1,STRING_NUM);
			memcpy(disp_data[5],da1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 6://充电电流过小
			memcpy(disp_data[0],chong1,STRING_NUM);
			memcpy(disp_data[1],dian1,STRING_NUM);
			memcpy(disp_data[2],dian1,STRING_NUM);
			memcpy(disp_data[3],liu1,STRING_NUM);
			memcpy(disp_data[4],guo1,STRING_NUM);
			memcpy(disp_data[5],xiao1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 7: //维护电流异常 
			memcpy(disp_data[0],wei1,STRING_NUM);
			memcpy(disp_data[1],hu1,STRING_NUM);
			memcpy(disp_data[2],dian1,STRING_NUM);
			memcpy(disp_data[3],liu1,STRING_NUM);
			memcpy(disp_data[4],yi1,STRING_NUM);
			memcpy(disp_data[5],chang1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 8://单体电压过高
			memcpy(disp_data[0],dan1,STRING_NUM);
			memcpy(disp_data[1],ti1,STRING_NUM);
			memcpy(disp_data[2],dian1,STRING_NUM);
			memcpy(disp_data[3],ya1,STRING_NUM);
			memcpy(disp_data[4],guo1,STRING_NUM);
			memcpy(disp_data[5],gao1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 9://单体电压过低
			memcpy(disp_data[0],dan1,STRING_NUM);
			memcpy(disp_data[1],ti1,STRING_NUM);
			memcpy(disp_data[2],dian1,STRING_NUM);
			memcpy(disp_data[3],ya1,STRING_NUM);
			memcpy(disp_data[4],guo1,STRING_NUM);
			memcpy(disp_data[5],di1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 10://电池组电压过高
			memcpy(disp_data[0],dian1,STRING_NUM);
			memcpy(disp_data[1],chi1,STRING_NUM);
			memcpy(disp_data[2],zu1,STRING_NUM);
			memcpy(disp_data[3],dian1,STRING_NUM);
			memcpy(disp_data[4],ya1,STRING_NUM);
			memcpy(disp_data[5],guo1,STRING_NUM);
			memcpy(disp_data[6],gao1,STRING_NUM);
			break;
		case 11://电池组电压过低
			memcpy(disp_data[0],dian1,STRING_NUM);
			memcpy(disp_data[1],chi1,STRING_NUM);
			memcpy(disp_data[2],zu1,STRING_NUM);
			memcpy(disp_data[3],dian1,STRING_NUM);
			memcpy(disp_data[4],ya1,STRING_NUM);
			memcpy(disp_data[5],guo1,STRING_NUM);
			memcpy(disp_data[6],di1,STRING_NUM);			
			break;
		case 12://充电电压过高
			memcpy(disp_data[0],chong1,STRING_NUM);
			memcpy(disp_data[1],dian1,STRING_NUM);
			memcpy(disp_data[2],dian1,STRING_NUM);
			memcpy(disp_data[3],ya1,STRING_NUM);
			memcpy(disp_data[4],guo1,STRING_NUM);
			memcpy(disp_data[5],gao1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 13://充电电压过低
			memcpy(disp_data[0],chong1,STRING_NUM);
			memcpy(disp_data[1],dian1,STRING_NUM);
			memcpy(disp_data[2],dian1,STRING_NUM);
			memcpy(disp_data[3],ya1,STRING_NUM);
			memcpy(disp_data[4],guo1,STRING_NUM);
			memcpy(disp_data[5],di1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 14://电池组温度过高 
			memcpy(disp_data[0],dian1,STRING_NUM);
			memcpy(disp_data[1],chi1,STRING_NUM);
			memcpy(disp_data[2],zu1,STRING_NUM);
			memcpy(disp_data[3],wen1,STRING_NUM);
			memcpy(disp_data[4],du1,STRING_NUM);
			memcpy(disp_data[5],guo1,STRING_NUM);
			memcpy(disp_data[6],gao1,STRING_NUM);
			break;
		case 15://电池组温度过低 
			memcpy(disp_data[0],dian1,STRING_NUM);
			memcpy(disp_data[1],chi1,STRING_NUM);
			memcpy(disp_data[2],zu1,STRING_NUM);
			memcpy(disp_data[3],wen1,STRING_NUM);
			memcpy(disp_data[4],du1,STRING_NUM);
			memcpy(disp_data[5],guo1,STRING_NUM);
			memcpy(disp_data[6],di1,STRING_NUM);
			break;
		case 16://充电机过温
			memcpy(disp_data[0],chong1,STRING_NUM);
			memcpy(disp_data[1],dian1,STRING_NUM);
			memcpy(disp_data[2],ji1,STRING_NUM);
			memcpy(disp_data[3],guo1,STRING_NUM);
			memcpy(disp_data[4],wen1,STRING_NUM);
			memcpy(disp_data[5],heiping,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 17://加热模块错误
			memcpy(disp_data[0],jia1,STRING_NUM);
			memcpy(disp_data[1],re1,STRING_NUM);
			memcpy(disp_data[2],mo1,STRING_NUM);
			memcpy(disp_data[3],kuai2,STRING_NUM);
			memcpy(disp_data[4],cuo1,STRING_NUM);
			memcpy(disp_data[5],wu1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 18://充电时间过长
			memcpy(disp_data[0],chong1,STRING_NUM);
			memcpy(disp_data[1],dian1,STRING_NUM);
			memcpy(disp_data[2],shi2,STRING_NUM);
			memcpy(disp_data[3],jian3,STRING_NUM);
			memcpy(disp_data[4],guo1,STRING_NUM);
			memcpy(disp_data[5],chang2,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 19://环境温度过高
			memcpy(disp_data[0],huan2,STRING_NUM);
			memcpy(disp_data[1],jing1,STRING_NUM);
			memcpy(disp_data[2],wen1,STRING_NUM);
			memcpy(disp_data[3],du1,STRING_NUM);
			memcpy(disp_data[4],guo1,STRING_NUM);
			memcpy(disp_data[5],gao1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 20://环境温度过低 
			memcpy(disp_data[0],huan2,STRING_NUM);
			memcpy(disp_data[1],jing1,STRING_NUM);
			memcpy(disp_data[2],wen1,STRING_NUM);
			memcpy(disp_data[3],du1,STRING_NUM);
			memcpy(disp_data[4],guo1,STRING_NUM);
			memcpy(disp_data[5],di1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 21://充电通讯异常
			memcpy(disp_data[0],chong1,STRING_NUM);
			memcpy(disp_data[1],dian1,STRING_NUM);
			memcpy(disp_data[2],tong1,STRING_NUM);
			memcpy(disp_data[3],xun1,STRING_NUM);
			memcpy(disp_data[4],yi1,STRING_NUM);
			memcpy(disp_data[5],chang1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		case 22://均衡通讯异常 
			memcpy(disp_data[0],jun1,STRING_NUM);
			memcpy(disp_data[1],heng1,STRING_NUM);
			memcpy(disp_data[2],tong1,STRING_NUM);
			memcpy(disp_data[3],xun1,STRING_NUM);
			memcpy(disp_data[4],yi1,STRING_NUM);
			memcpy(disp_data[5],chang1,STRING_NUM);
			memcpy(disp_data[6],heiping,STRING_NUM);
			break;
		default:
			break;
	}
	color_font = 1; //不反白
	for(i=0;i<WORDS_NUM;i++) //循环的将每个字一显示
	{
		display_graphic1_12x12(line_num,(1+12*i+12),disp_data[i],color_font); //显示10个字。i=0...9 1+12*i+8列
	}
}
