#ifndef CHARGE_FSM_H
#define CHARGE_FSM_H

#define BIT(x)  				  (1L << (x))
#define EVENT(id) 				  BIT(id)
#define checkEventID(event, id)   ((event) & EVENT(id))


extern volatile uint32_t g_fsm_disp_event;


//有可能在一个状态下，发生好几个事件。状态结束，是否结束事件
//不同的任务或者状态机，可以定义不同的事件集（enum，一个事件集最多32个事件）
//define the Task priority 
typedef enum
{
    eKeySelect = 0, 		//选择按键事件
    eKeyConfirm,    		//确认按键事件
	eBatChargeIng,			//电池标充电进行事件	
	eBatInfo,				//查看电池信息事件	
}DispTaskEvent_t;

typedef enum
{	
	STATE_SELECT_1,						  	//行1 突出显示	
	STATE_SELECT_2,							//行2 突出显示
	STATE_SELECT_3,							//行3 突出显示
	STATE_BATTERY_CHARGE, 				    //电池标充电界面	
	STATE_BATTERY_INFO,                     //查看电池信息界面
	STATE_VERSION_INFO, 					//版本型号
}STATE;


void display_task(void);
void SET_EVENT(uint32_t event_id);
void  CRC_EVENT(uint32_t event_id);
uint32_t GET_EVENT(void);
#endif

