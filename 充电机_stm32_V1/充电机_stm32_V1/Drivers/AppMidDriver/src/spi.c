#include "spi.h"
/*
CS: C0
SCK:C1
SDI:C2
DC: C3
RST:C4
*/
#define OLED_CS_Pin GPIO_PIN_0
#define OLED_CS_GPIO_Port GPIOC
#define OLED_SCK_Pin GPIO_PIN_1
#define OLED_SCK_GPIO_Port GPIOC
#define OLED_SDI_Pin GPIO_PIN_2
#define OLED_SDI_GPIO_Port GPIOC


void SPI_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();


	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, OLED_CS_Pin|OLED_SCK_Pin|OLED_SDI_Pin, GPIO_PIN_SET);

	/*Configure GPIO pins : PCPin PCPin PCPin */
	GPIO_InitStruct.Pin = OLED_CS_Pin|OLED_SCK_Pin|OLED_SDI_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
}

void SPI_Write(uint8_t wr_data)
{
	uint8_t i = 0;
	
	HAL_GPIO_WritePin(GPIOC, OLED_CS_Pin, GPIO_PIN_RESET);
	
	for(i=0;i<8;i++)
	{
		HAL_GPIO_WritePin(GPIOC, OLED_SCK_Pin, GPIO_PIN_RESET);
		
		/*�ȴ���λ*/
		if (wr_data & 0x80) 
		{
			HAL_GPIO_WritePin(GPIOC, OLED_SDI_Pin, GPIO_PIN_SET);
		}
		else 
		{
			HAL_GPIO_WritePin(GPIOC, OLED_SDI_Pin, GPIO_PIN_RESET);
		}
		
		HAL_GPIO_WritePin(GPIOC, OLED_SCK_Pin, GPIO_PIN_SET);
		
		wr_data <<= 1;
	}
	
	HAL_GPIO_WritePin(GPIOC, OLED_CS_Pin, GPIO_PIN_SET);
}



