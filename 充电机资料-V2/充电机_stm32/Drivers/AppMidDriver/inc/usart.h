/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usart_H
#define __usart_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

#define MAX_RECV_LEN			100
#define USART1_MAX_RECV_LEN		MAX_RECV_LEN

typedef enum
{
	USART1_RX_STATE_START = 0U,
	USART1_RX_STATE_FINISH,
}USART1_RX_STATE;

extern uint8_t usart1_rx_len;
extern uint8_t usart1_rx_state;
extern uint8_t tmp_usart1_rx_buffer[USART1_MAX_RECV_LEN];
extern uint8_t g_usart1_rx_buff[USART1_MAX_RECV_LEN];

extern UART_HandleTypeDef huart1;

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void MX_USART1_UART_Init(void);
void USART1_TX(uint8_t *pData, uint16_t Size);

#ifdef __cplusplus
}
#endif
#endif /*__ usart_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
