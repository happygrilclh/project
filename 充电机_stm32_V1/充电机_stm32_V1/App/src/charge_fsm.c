#include "global.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "usart.h"
#include "charge_fsm.h"
#include "drive_oled.h"
#include "tim.h"


volatile uint32_t g_fsm_disp_event = 0x00; //最多32个事件
uint8_t fsm_disp_state = STATE_SELECT_1;

extern void drive_display(uint8 STATE);

uint32_t GET_EVENT(void)
{
	uint32_t tmp_event = g_fsm_disp_event;
	
	g_fsm_disp_event = 0;
	
	return tmp_event;
}


/*
设置事件，是设置总事件的某一位。
读取事件，将事件读取后，要将该事件位清除，表示已处理掉该位的事件。
*/
void  SET_EVENT(uint32_t event_id)
{
	g_fsm_disp_event |= BIT(event_id);
}

/*
清除事件，清除总事件的某一位。
*/
void  CRC_EVENT(uint32_t event_id)
{
	g_fsm_disp_event &= ~BIT(event_id);
}



void display_task(void)
{
	volatile uint32_t tmp_event = 0;
	
	tmp_event = GET_EVENT();
		
	switch(fsm_disp_state)
	{
		case STATE_SELECT_1:
			 drive_display(STATE_SELECT_1); //行1 突出显示
		
			if(checkEventID(tmp_event, eKeySelect))
			{
				fsm_disp_state = STATE_SELECT_2;
				return; //每次一个状态下，只能处理一个事件，处理完此事件后，要把事件清0，否则有可能处理多次事件，引起逻辑错误。
			}

			
			if(checkEventID(tmp_event, eKeyConfirm))
			{
				clear_full_screen();
				fsm_disp_state = STATE_BATTERY_CHARGE;
				return;
			}
			
			
		break;
		case STATE_SELECT_2:
			drive_display(STATE_SELECT_2); //行2 突出显示
					
			if(checkEventID(tmp_event, eKeySelect))
			{
				fsm_disp_state = STATE_SELECT_3;
				return;
			}
		
			if(checkEventID(tmp_event, eKeyConfirm))
			{
				clear_full_screen();
				fsm_disp_state = STATE_BATTERY_INFO;
				return;					
			}
			
		break;
		case STATE_SELECT_3:
			drive_display(STATE_SELECT_3); //行3 突出显示		
		
			if(checkEventID(tmp_event, eKeySelect))
			{
				fsm_disp_state = STATE_SELECT_1;
				return;
			}
			
			if(checkEventID(tmp_event, eKeyConfirm))
			{
				clear_full_screen();
				fsm_disp_state = STATE_VERSION_INFO;
				return;
						
			}
			
		break;
		case STATE_BATTERY_INFO:
			 drive_display(STATE_BATTERY_INFO);	
			
			if(checkEventID(tmp_event, eKeySelect))
			{
				clear_full_screen();
				fsm_disp_state = STATE_SELECT_2;
				return;
			}
							
		break;
		case STATE_BATTERY_CHARGE:
			 drive_display(STATE_BATTERY_CHARGE);

		
			if(checkEventID(tmp_event, eKeySelect))
			{
				clear_full_screen();
				fsm_disp_state = STATE_SELECT_1;
				return;
			}
						
	
				
		break;
		case STATE_VERSION_INFO :	//版本界面
			 drive_display(STATE_VERSION_INFO);	 
		   	
			
			if(checkEventID(tmp_event, eKeySelect))
			{
					clear_full_screen();
					fsm_disp_state = STATE_SELECT_3;
				    return;
			}
			
		break;
		default:
		break;	
	}
}
