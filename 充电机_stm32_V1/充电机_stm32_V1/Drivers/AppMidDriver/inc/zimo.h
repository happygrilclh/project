#ifndef _ZIMO_H
#define _ZIMO_H


extern const unsigned char ascii_table_8x16[95][16]={
/*--  文字:     --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,


/*--  文字:  !  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0xF8,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x33,0x30,0x00,0x00,0x00,

/*--  文字:  "  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x10,0x0C,0x06,0x10,0x0C,0x06,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*--  文字:  #  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x40,0xC0,0x78,0x40,0xC0,0x78,0x40,0x00,0x04,0x3F,0x04,0x04,0x3F,0x04,0x04,0x00,

/*--  文字:  $  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x70,0x88,0xFC,0x08,0x30,0x00,0x00,0x00,0x18,0x20,0xFF,0x21,0x1E,0x00,0x00,

/*--  文字:  %  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0xF0,0x08,0xF0,0x00,0xE0,0x18,0x00,0x00,0x00,0x21,0x1C,0x03,0x1E,0x21,0x1E,0x00,

/*--  文字:  &  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0xF0,0x08,0x88,0x70,0x00,0x00,0x00,0x1E,0x21,0x23,0x24,0x19,0x27,0x21,0x10,

/*--  文字:  '  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x10,0x16,0x0E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*--  文字:  (  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0xE0,0x18,0x04,0x02,0x00,0x00,0x00,0x00,0x07,0x18,0x20,0x40,0x00,

/*--  文字:  )  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x02,0x04,0x18,0xE0,0x00,0x00,0x00,0x00,0x40,0x20,0x18,0x07,0x00,0x00,0x00,

/*--  文字:  *  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x40,0x40,0x80,0xF0,0x80,0x40,0x40,0x00,0x02,0x02,0x01,0x0F,0x01,0x02,0x02,0x00,

/*--  文字:  +  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0xF0,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x1F,0x01,0x01,0x01,0x00,

/*--  文字:  ,  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0xB0,0x70,0x00,0x00,0x00,0x00,0x00,

/*--  文字:  -  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,

/*--  文字:  .  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x30,0x30,0x00,0x00,0x00,0x00,0x00,

/*--  文字:  /  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x00,0x80,0x60,0x18,0x04,0x00,0x60,0x18,0x06,0x01,0x00,0x00,0x00,

/*--  文字:  0  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0xE0,0x10,0x08,0x08,0x10,0xE0,0x00,0x00,0x0F,0x10,0x20,0x20,0x10,0x0F,0x00,

/*--  文字:  1  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x10,0x10,0xF8,0x00,0x00,0x00,0x00,0x00,0x20,0x20,0x3F,0x20,0x20,0x00,0x00,

/*--  文字:  2  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x70,0x08,0x08,0x08,0x88,0x70,0x00,0x00,0x30,0x28,0x24,0x22,0x21,0x30,0x00,

/*--  文字:  3  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x30,0x08,0x88,0x88,0x48,0x30,0x00,0x00,0x18,0x20,0x20,0x20,0x11,0x0E,0x00,

/*--  文字:  4  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0xC0,0x20,0x10,0xF8,0x00,0x00,0x00,0x07,0x04,0x24,0x24,0x3F,0x24,0x00,

/*--  文字:  5  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0xF8,0x08,0x88,0x88,0x08,0x08,0x00,0x00,0x19,0x21,0x20,0x20,0x11,0x0E,0x00,

/*--  文字:  6  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0xE0,0x10,0x88,0x88,0x18,0x00,0x00,0x00,0x0F,0x11,0x20,0x20,0x11,0x0E,0x00,

/*--  文字:  7  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x38,0x08,0x08,0xC8,0x38,0x08,0x00,0x00,0x00,0x00,0x3F,0x00,0x00,0x00,0x00,

/*--  文字:  8  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x70,0x88,0x08,0x08,0x88,0x70,0x00,0x00,0x1C,0x22,0x21,0x21,0x22,0x1C,0x00,

/*--  文字:  9  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0xE0,0x10,0x08,0x08,0x10,0xE0,0x00,0x00,0x00,0x31,0x22,0x22,0x11,0x0F,0x00,

/*--  文字:  :  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0xC0,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x30,0x30,0x00,0x00,0x00,

/*--  文字:  ;  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x60,0x00,0x00,0x00,0x00,

/*--  文字:  <  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x80,0x40,0x20,0x10,0x08,0x00,0x00,0x01,0x02,0x04,0x08,0x10,0x20,0x00,

/*--  文字:  =  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x00,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x00,

/*--  文字:  >  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x08,0x10,0x20,0x40,0x80,0x00,0x00,0x00,0x20,0x10,0x08,0x04,0x02,0x01,0x00,

/*--  文字:  ?  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x70,0x48,0x08,0x08,0x08,0xF0,0x00,0x00,0x00,0x00,0x30,0x36,0x01,0x00,0x00,

/*--  文字:  @  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0xC0,0x30,0xC8,0x28,0xE8,0x10,0xE0,0x00,0x07,0x18,0x27,0x24,0x23,0x14,0x0B,0x00,

/*--  文字:  A  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0xC0,0x38,0xE0,0x00,0x00,0x00,0x20,0x3C,0x23,0x02,0x02,0x27,0x38,0x20,

/*--  文字:  B  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x88,0x88,0x88,0x70,0x00,0x00,0x20,0x3F,0x20,0x20,0x20,0x11,0x0E,0x00,

/*--  文字:  C  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0xC0,0x30,0x08,0x08,0x08,0x08,0x38,0x00,0x07,0x18,0x20,0x20,0x20,0x10,0x08,0x00,

/*--  文字:  D  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x08,0x08,0x08,0x10,0xE0,0x00,0x20,0x3F,0x20,0x20,0x20,0x10,0x0F,0x00,

/*--  文字:  E  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x88,0x88,0xE8,0x08,0x10,0x00,0x20,0x3F,0x20,0x20,0x23,0x20,0x18,0x00,

/*--  文字:  F  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x88,0x88,0xE8,0x08,0x10,0x00,0x20,0x3F,0x20,0x00,0x03,0x00,0x00,0x00,

/*--  文字:  G  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0xC0,0x30,0x08,0x08,0x08,0x38,0x00,0x00,0x07,0x18,0x20,0x20,0x22,0x1E,0x02,0x00,

/*--  文字:  H  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x08,0x00,0x00,0x08,0xF8,0x08,0x20,0x3F,0x21,0x01,0x01,0x21,0x3F,0x20,

/*--  文字:  I  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x08,0x08,0xF8,0x08,0x08,0x00,0x00,0x00,0x20,0x20,0x3F,0x20,0x20,0x00,0x00,

/*--  文字:  J  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x08,0x08,0xF8,0x08,0x08,0x00,0xC0,0x80,0x80,0x80,0x7F,0x00,0x00,0x00,

/*--  文字:  K  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x88,0xC0,0x28,0x18,0x08,0x00,0x20,0x3F,0x20,0x01,0x26,0x38,0x20,0x00,

/*--  文字:  L  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x08,0x00,0x00,0x00,0x00,0x00,0x20,0x3F,0x20,0x20,0x20,0x20,0x30,0x00,

/*--  文字:  M  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0xF8,0x00,0xF8,0xF8,0x08,0x00,0x20,0x3F,0x00,0x3F,0x00,0x3F,0x20,0x00,

/*--  文字:  N  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x30,0xC0,0x00,0x08,0xF8,0x08,0x20,0x3F,0x20,0x00,0x07,0x18,0x3F,0x00,

/*--  文字:  O  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0xE0,0x10,0x08,0x08,0x08,0x10,0xE0,0x00,0x0F,0x10,0x20,0x20,0x20,0x10,0x0F,0x00,

/*--  文字:  P  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x08,0x08,0x08,0x08,0xF0,0x00,0x20,0x3F,0x21,0x01,0x01,0x01,0x00,0x00,

/*--  文字:  Q  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0xE0,0x10,0x08,0x08,0x08,0x10,0xE0,0x00,0x0F,0x18,0x24,0x24,0x38,0x50,0x4F,0x00,

/*--  文字:  R  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x88,0x88,0x88,0x88,0x70,0x00,0x20,0x3F,0x20,0x00,0x03,0x0C,0x30,0x20,

/*--  文字:  S  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x70,0x88,0x08,0x08,0x08,0x38,0x00,0x00,0x38,0x20,0x21,0x21,0x22,0x1C,0x00,

/*--  文字:  T  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x18,0x08,0x08,0xF8,0x08,0x08,0x18,0x00,0x00,0x00,0x20,0x3F,0x20,0x00,0x00,0x00,

/*--  文字:  U  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x08,0x00,0x00,0x08,0xF8,0x08,0x00,0x1F,0x20,0x20,0x20,0x20,0x1F,0x00,

/*--  文字:  V  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0x78,0x88,0x00,0x00,0xC8,0x38,0x08,0x00,0x00,0x07,0x38,0x0E,0x01,0x00,0x00,

/*--  文字:  W  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0xF8,0x08,0x00,0xF8,0x00,0x08,0xF8,0x00,0x03,0x3C,0x07,0x00,0x07,0x3C,0x03,0x00,

/*--  文字:  X  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0x18,0x68,0x80,0x80,0x68,0x18,0x08,0x20,0x30,0x2C,0x03,0x03,0x2C,0x30,0x20,

/*--  文字:  Y  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0x38,0xC8,0x00,0xC8,0x38,0x08,0x00,0x00,0x00,0x20,0x3F,0x20,0x00,0x00,0x00,

/*--  文字:  Z  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x10,0x08,0x08,0x08,0xC8,0x38,0x08,0x00,0x20,0x38,0x26,0x21,0x20,0x20,0x18,0x00,

/*--  文字:  [  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0xFE,0x02,0x02,0x02,0x00,0x00,0x00,0x00,0x7F,0x40,0x40,0x40,0x00,

/*--  文字:  \  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x0C,0x30,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x06,0x38,0xC0,0x00,

/*--  文字:  ]  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x02,0x02,0x02,0xFE,0x00,0x00,0x00,0x00,0x40,0x40,0x40,0x7F,0x00,0x00,0x00,

/*--  文字:  ^  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x04,0x02,0x02,0x02,0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*--  文字:  _  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,

/*--  文字:  `  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x02,0x02,0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*--  文字:  a  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x80,0x80,0x80,0x80,0x00,0x00,0x00,0x19,0x24,0x22,0x22,0x22,0x3F,0x20,

/*--  文字:  b  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x00,0x80,0x80,0x00,0x00,0x00,0x00,0x3F,0x11,0x20,0x20,0x11,0x0E,0x00,

/*--  文字:  c  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x80,0x80,0x80,0x00,0x00,0x00,0x0E,0x11,0x20,0x20,0x20,0x11,0x00,

/*--  文字:  d  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x80,0x80,0x88,0xF8,0x00,0x00,0x0E,0x11,0x20,0x20,0x10,0x3F,0x20,

/*--  文字:  e  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x80,0x80,0x80,0x80,0x00,0x00,0x00,0x1F,0x22,0x22,0x22,0x22,0x13,0x00,

/*--  文字:  f  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x80,0x80,0xF0,0x88,0x88,0x88,0x18,0x00,0x20,0x20,0x3F,0x20,0x20,0x00,0x00,

/*--  文字:  g  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x80,0x80,0x80,0x80,0x80,0x00,0x00,0x6B,0x94,0x94,0x94,0x93,0x60,0x00,

/*--  文字:  h  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x00,0x80,0x80,0x80,0x00,0x00,0x20,0x3F,0x21,0x00,0x00,0x20,0x3F,0x20,

/*--  文字:  i  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x80,0x98,0x98,0x00,0x00,0x00,0x00,0x00,0x20,0x20,0x3F,0x20,0x20,0x00,0x00,

/*--  文字:  j  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x80,0x98,0x98,0x00,0x00,0x00,0xC0,0x80,0x80,0x80,0x7F,0x00,0x00,

/*--  文字:  k  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x08,0xF8,0x00,0x00,0x80,0x80,0x80,0x00,0x20,0x3F,0x24,0x02,0x2D,0x30,0x20,0x00,

/*--  文字:  l  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x08,0x08,0xF8,0x00,0x00,0x00,0x00,0x00,0x20,0x20,0x3F,0x20,0x20,0x00,0x00,

/*--  文字:  m  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x00,0x20,0x3F,0x20,0x00,0x3F,0x20,0x00,0x3F,

/*--  文字:  n  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x80,0x80,0x00,0x80,0x80,0x80,0x00,0x00,0x20,0x3F,0x21,0x00,0x00,0x20,0x3F,0x20,

/*--  文字:  o  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x80,0x80,0x80,0x80,0x00,0x00,0x00,0x1F,0x20,0x20,0x20,0x20,0x1F,0x00,

/*--  文字:  p  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x80,0x80,0x00,0x80,0x80,0x00,0x00,0x00,0x80,0xFF,0xA1,0x20,0x20,0x11,0x0E,0x00,

/*--  文字:  q  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x80,0x80,0x80,0x80,0x00,0x00,0x0E,0x11,0x20,0x20,0xA0,0xFF,0x80,

/*--  文字:  r  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x80,0x80,0x80,0x00,0x80,0x80,0x80,0x00,0x20,0x20,0x3F,0x21,0x20,0x00,0x01,0x00,

/*--  文字:  s  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x80,0x80,0x80,0x80,0x80,0x00,0x00,0x33,0x24,0x24,0x24,0x24,0x19,0x00,

/*--  文字:  t  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x80,0x80,0xE0,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x1F,0x20,0x20,0x00,0x00,

/*--  文字:  u  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x80,0x80,0x00,0x00,0x00,0x80,0x80,0x00,0x00,0x1F,0x20,0x20,0x20,0x10,0x3F,0x20,

/*--  文字:  v  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x80,0x80,0x80,0x00,0x00,0x80,0x80,0x80,0x00,0x01,0x0E,0x30,0x08,0x06,0x01,0x00,

/*--  文字:  w  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x80,0x80,0x00,0x80,0x00,0x80,0x80,0x80,0x0F,0x30,0x0C,0x03,0x0C,0x30,0x0F,0x00,

/*--  文字:  x  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x80,0x80,0x00,0x80,0x80,0x80,0x00,0x00,0x20,0x31,0x2E,0x0E,0x31,0x20,0x00,

/*--  文字:  y  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x80,0x80,0x80,0x00,0x00,0x80,0x80,0x80,0x80,0x81,0x8E,0x70,0x18,0x06,0x01,0x00,

/*--  文字:  z  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x80,0x80,0x80,0x80,0x80,0x80,0x00,0x00,0x21,0x30,0x2C,0x22,0x21,0x30,0x00,

/*--  文字:  {  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x00,0x80,0x7C,0x02,0x02,0x00,0x00,0x00,0x00,0x00,0x3F,0x40,0x40,

/*--  文字:  |  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x00,0x00,0x00,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x00,0x00,0x00,

/*--  文字:  }  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x02,0x02,0x7C,0x80,0x00,0x00,0x00,0x00,0x40,0x40,0x3F,0x00,0x00,0x00,0x00,

/*--  文字:  ~  --*/
/*--  Comic Sans MS12;  此字体下对应的点阵为：宽x高=8x16   --*/
0x00,0x06,0x01,0x01,0x02,0x02,0x04,0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00

};

  const unsigned char huan1[]={
/*--  文字:  欢  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      0x04,0x24,0x44,0x84,0x64,0x9C,0x40,0x30,0x0F,0xC8,0x08,0x08,0x28,0x18,0x00,0x00,
      0x10,0x08,0x06,0x01,0x82,0x4C,0x20,0x18,0x06,0x01,0x06,0x18,0x20,0x40,0x80,0x00};
  const unsigned char ying1[]={
/*--  文字:  迎  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      0x40,0x40,0x42,0xCC,0x00,0x00,0xFC,0x04,0x02,0x00,0xFC,0x04,0x04,0xFC,0x00,0x00,
      0x00,0x40,0x20,0x1F,0x20,0x40,0x4F,0x44,0x42,0x40,0x7F,0x42,0x44,0x43,0x40,0x00};
const unsigned char shi1[]={
/*--  文字:  使  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      0x80,0x60,0xF8,0x07,0x04,0xE4,0x24,0x24,0x24,0xFF,0x24,0x24,0x24,0xE4,0x04,0x00,
      0x00,0x00,0xFF,0x00,0x80,0x81,0x45,0x29,0x11,0x2F,0x41,0x41,0x81,0x81,0x80,0x00

};
const unsigned char yong1[]={
/*--  文字:  用  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      0x00,0x00,0xFE,0x22,0x22,0x22,0x22,0xFE,0x22,0x22,0x22,0x22,0xFE,0x00,0x00,0x00,
      0x80,0x60,0x1F,0x02,0x02,0x02,0x02,0x7F,0x02,0x02,0x42,0x82,0x7F,0x00,0x00,0x00};

 const unsigned char xing1[]={
/*--  文字:  型  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
      0x90,0x52,0x3E,0x12,0x12,0x7E,0x12,0x00,0x3E,0x80,0xFF,0x00,0x08,0x0A,0x0A,0x0A,
	  0x0A,0x0F,0x0A,0x0A,0x0A,0x0A,0x08,0x00};/*"型",0*/
const unsigned char li1[]={
/*--  文字:  锂  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
      0x98,0xF7,0x94,0x94,0x00,0x7F,0x49,0xFF,0x49,0x7F,0x00,0x00,0x00,0x0F,0x04,0x02,
      0x08,0x09,0x09,0x0F,0x09,0x09,0x08,0x00};
const unsigned char dian1[]={
/*--  文字:  电  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      0xFC,0x24,0x24,0x24,0xFF,0x24,0x24,0x24,0xFC,0x00,0x00,0x00,0x03,0x01,0x01,0x01,
      0x07,0x09,0x09,0x09,0x09,0x08,0x0E,0x00};
const unsigned char chi1[]={
/*--  文字:  池  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
     0x22,0x44,0x40,0xFC,0x20,0x10,0xFF,0x08,0x04,0xFC,0x00,0x00,0x04,0x02,0x00,0x07,
     0x08,0x08,0x0B,0x08,0x09,0x09,0x0C,0x00};
const unsigned char zhi1[]={
/*--  文字:  智  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      0x94,0x53,0xB2,0x9E,0xB2,0xD2,0x80,0xBE,0xA2,0xA2,0x3E,0x00,0x00,0x00,0x0F,0x0A,
      0x0A,0x0A,0x0A,0x0A,0x0A,0x0F,0x00,0x00};
const unsigned char neng1[]={
/*--  文字:  能  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      0x04,0xF6,0x55,0x54,0xF6,0x00,0xCF,0x14,0x12,0x91,0x18,0x00,0x00,0x0F,0x01,0x09,
      0x0F,0x00,0x07,0x0A,0x09,0x08,0x0E,0x00};
const unsigned char chong1[]={
/*--  文字: 充  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      0x04,0x44,0x64,0xD4,0x4D,0x46,0x44,0xD4,0x64,0xC4,0x04,0x00,0x08,0x08,0x04,0x03,
      0x00,0x00,0x00,0x07,0x08,0x08,0x0E,0x00};


const unsigned char ji1[] = {0x98,0xD4,0xB3,0x88,0x02,0xFE,0x82,0x02,0x32,0x2E,0xE0,0x00,0x04,0x04,0x02,0x0A,0x06,0x01,0x08,0x05,0x02,0x05,0x08,0x00};/*"级",0*/

 const unsigned char gong1[]={
/*--  文字: 功  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      /*--  文字:  功  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x04,0x04,0xFC,0x04,0x04,0x08,0xFF,0x08,0x08,0x08,0xF8,0x00,0x02,0x02,0x01,0x09,
    0x05,0x03,0x00,0x00,0x08,0x08,0x07,0x00};

const unsigned char xuan1[]={
/*--  文字:  选  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x10,0x11,0xF2,0x00,0x28,0x26,0xE4,0x3F,0xE4,0x24,0x20,0x00,0x08,0x04,0x03,0x04,
    0x0A,0x09,0x08,0x08,0x09,0x0A,0x0B,0x00};
const unsigned char ze1[]={
/*--  文字:  择  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x88,0x88,0xFF,0x48,0x21,0xA3,0x95,0xE9,0x95,0xA3,0x20,0x00,0x00,0x08,0x0F,0x00,
    0x02,0x02,0x02,0x0F,0x02,0x02,0x02,0x00};

 const unsigned char dot1[]={
/*--  文字:  、  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0x00,0x00,0x40,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x01,0x03,0x00,0x00,0x00,0x00,0x00,0x00};

const unsigned char wei1[]={
/*--  文字:  维  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x98,0xD4,0xB3,0x88,0x10,0xFC,0x27,0x24,0xFD,0x26,0x24,0x00,0x04,0x04,0x02,0x02,
    0x00,0x0F,0x09,0x09,0x0F,0x09,0x09,0x00};

const unsigned char hu1[]={
/*--  文字:  护  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x88,0x88,0xFF,0x48,0x00,0xFC,0x44,0x45,0x46,0x44,0xFC,0x00,0x00,0x08,0x0F,0x00,
    0x08,0x07,0x00,0x00,0x00,0x00,0x00,0x00};

const unsigned char check1[]={
/*--  文字:  查  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x12,0x12,0xEA,0xA6,0xA2,0xBF,0xA2,0xA6,0xEA,0x12,0x12,0x00,0x08,0x08,0x0B,0x0A,
    0x0A,0x0A,0x0A,0x0A,0x0B,0x08,0x08,0x00};

const unsigned char kan1[]={

/*--  文字:  看  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x20,0xAA,0xEA,0xAA,0xBA,0xAE,0xA9,0xA9,0xA9,0xE9,0x20,0x00,0x01,0x00,0x0F,0x0A,
    0x0A,0x0A,0x0A,0x0A,0x0A,0x0F,0x00,0x00};

const unsigned char xin1[]={
/*--  文字:  信  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x10,0xFC,0x03,0x04,0x54,0x54,0x55,0x56,0x54,0x54,0x04,0x00,0x00,0x0F,0x00,0x00,
    0x0F,0x05,0x05,0x05,0x05,0x0F,0x00,0x00};

const unsigned char xi1[]={
/*--  文字:  息  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0xFE,0xAA,0xAA,0xAB,0xAA,0xAA,0xAA,0xAA,0xFE,0x00,0x00,0x08,0x06,0x00,0x06,
    0x08,0x09,0x0A,0x08,0x0C,0x02,0x0C,0x00};

const unsigned char shuzi1[]={0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x00,0x00,0x00,0x00,0x00,
                     0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};	/*"一",0*/
const unsigned char shuzi2[]={0x00,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x00,0x00,0x04,0x04,0x04,0x04,
                     0x04,0x04,0x04,0x04,0x04,0x04,0x04,0x00}; /*"二",0*/
const unsigned char shuzi3[]={0x00,0x02,0x42,0x42,0x42,0x42,0x42,0x42,0x42,0x02,0x00,0x00,0x08,0x08,0x08,0x08,
                     0x08,0x08,0x08,0x08,0x08,0x08,0x08,0x00};/*"三",0*/
const unsigned char shuzi4[]={0x00,0xFE,0x02,0x82,0x7E,0x02,0x02,0x7E,0x82,0x82,0xFE,0x00,0x00,0x0F,0x05,0x04,
                     0x04,0x04,0x04,0x04,0x04,0x04,0x0F,0x00};/*"四",0*/
const unsigned char shuzi5[]={0x02,0x22,0x22,0xE2,0x3E,0x22,0x22,0x22,0xE2,0x02,0x00,0x00,0x08,0x08,0x0E,0x09,
                    0x08,0x08,0x08,0x08,0x0F,0x08,0x08,0x00};/*"五",0*/
const unsigned char zheng1[]={
/*--  文字:  正  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0x02,0xE2,0x02,0x02,0xFE,0x42,0x42,0x42,0x42,0x00,0x00,0x08,0x08,0x0F,0x08,
    0x08,0x0F,0x08,0x08,0x08,0x08,0x08,0x00};

const unsigned char zai1[]={
/*--  文字:  在  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x84,0x44,0xE4,0x1C,0x07,0x84,0x84,0xF4,0x84,0x84,0x04,0x00,0x00,0x00,0x0F,0x00,
    0x08,0x08,0x08,0x0F,0x08,0x08,0x08,0x00};

const unsigned char zong1[]={
/*--  文字:  总  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0x7C,0x45,0x46,0x44,0x44,0x44,0x46,0x45,0x7C,0x00,0x00,0x08,0x06,0x00,0x06,
    0x08,0x09,0x0A,0x08,0x0C,0x01,0x06,0x00};

const unsigned char ya1[]={
/*--  文字:  压  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0xFE,0x02,0x42,0x42,0x42,0xFA,0x42,0x42,0x42,0x02,0x00,0x08,0x07,0x08,0x08,
    0x08,0x08,0x0F,0x08,0x09,0x0A,0x08,0x00};

const unsigned char maohao1[]={
/*--  文字:  ：  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0x00,0x30,0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x03,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

const unsigned char liu1[]={
/*--  文字:  流  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x22,0x44,0x00,0x24,0xB4,0x2C,0xA5,0x26,0xA4,0x34,0x64,0x00,0x04,0x02,0x08,0x04,
    0x03,0x00,0x0F,0x00,0x07,0x08,0x0E,0x00};

const unsigned char wen1[]={
/*--  文字:  温  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x22,0x44,0x00,0xC0,0x5F,0xD5,0x55,0xD5,0x5F,0xC0,0x00,0x00,0x04,0x02,0x09,0x0F,
    0x08,0x0F,0x08,0x0F,0x08,0x0F,0x08,0x00};

const unsigned char du1[]={
/*--  文字:  度  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0xFE,0x0A,0x8A,0xBE,0xAA,0xAB,0xAA,0xBE,0x8A,0x0A,0x00,0x08,0x07,0x00,0x08,
    0x09,0x0A,0x04,0x04,0x0A,0x09,0x08,0x00};


 const unsigned char zui1[]={
/*--  文字:  最  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x10,0xF0,0x5F,0x55,0xF5,0x15,0x55,0xD5,0x5F,0xD0,0x10,0x00,0x04,0x07,0x05,0x05,
    0x0F,0x02,0x08,0x05,0x02,0x05,0x08,0x00};

const unsigned char da1[]={
/*--  文字:  大  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x10,0x10,0x10,0x10,0xD0,0x3F,0xD0,0x10,0x10,0x10,0x10,0x00,0x08,0x08,0x04,0x03,
    0x00,0x00,0x00,0x03,0x04,0x08,0x08,0x00};

const unsigned char cha1[]={
/*--  文字:  差  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
    0x44,0x54,0x55,0xD6,0x74,0x5C,0x54,0x56,0x55,0x54,0x44,0x00,0x04,0x02,0x09,0x09,
    0x09,0x09,0x0F,0x09,0x09,0x09,0x08,0x00};

const unsigned char wan1[]={
/*--  文字:  完  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x86,0x82,0x92,0x92,0x92,0x93,0x92,0x92,0x92,0x82,0x86,0x00,0x08,0x08,0x04,0x03,
    0x00,0x00,0x00,0x07,0x08,0x08,0x0C,0x00};

const unsigned char cheng1[]={
/*--  文字:  成  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0xFC,0x24,0x24,0xE4,0x04,0xFF,0x04,0x85,0x66,0x04,0x00,0x08,0x07,0x00,0x02,
    0x0B,0x04,0x02,0x01,0x02,0x04,0x0F,0x00};

const unsigned char dang1[]={
/*--  文字:  当  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0x12,0x94,0x90,0x90,0x9F,0x90,0x90,0x94,0xF2,0x00,0x00,0x00,0x04,0x04,0x04,
    0x04,0x04,0x04,0x04,0x04,0x0F,0x00,0x00};

const unsigned char qian1[]={
/*--  文字:  前  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x04,0xF4,0x55,0x56,0x54,0xF4,0x04,0xE6,0x05,0xF4,0x04,0x00,0x00,0x0F,0x01,0x01,
    0x09,0x0F,0x00,0x03,0x08,0x0F,0x00,0x00};

 const unsigned char gu1[]={
/*--  文字:故 --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
     0xC8,0x48,0x7F,0x48,0xC8,0x10,0xEF,0x08,0x08,0xF8,0x08,0x00,0x0F,0x04,0x04,0x04,
     0x0F,0x08,0x05,0x02,0x05,0x08,0x08,0x00};

const unsigned char zhang1[]={
/*--  文字:  障  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0xFE,0x02,0x32,0xCE,0x08,0xFA,0xAE,0xAB,0xAE,0xFA,0x08,0x00,0x0F,0x02,0x02,0x01,
    0x02,0x02,0x02,0x0F,0x02,0x02,0x02,0x00};



const unsigned char lei1[]={
/*--  文字:  类  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x08,0x48,0x2A,0x1C,0x08,0xBF,0x08,0x1C,0x2A,0x48,0x08,0x00,0x09,0x09,0x05,0x05,
    0x03,0x01,0x03,0x05,0x05,0x09,0x09,0x00};

const unsigned char x1[]={
/*--  文字:  型  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x90,0x52,0x3E,0x12,0x12,0x7E,0x12,0x00,0x3E,0x80,0xFF,0x00,0x08,0x0A,0x0A,0x0A,
	0x0A,0x0F,0x0A,0x0A,0x0A,0x0A,0x08,0x00};

  const unsigned char duan1[]={
/*--  文字:  断  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0xFC,0x92,0x54,0xFF,0x54,0x92,0x00,0xFE,0x12,0xF2,0x11,0x00,0x07,0x04,0x04,0x07,
	0x04,0x04,0x08,0x07,0x00,0x0F,0x00,0x00};

const unsigned char xian1[]={
/*--  文字:  线  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x98,0xD4,0xB3,0x88,0x00,0x48,0x48,0xFF,0x24,0xA5,0x26,0x00,0x04,0x04,0x02,0x02,
	0x08,0x08,0x04,0x03,0x05,0x08,0x0E,0x00};

const unsigned char uan1[]={
/*--  文字:  短  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x48,0x47,0xFC,0x44,0x02,0x7A,0x4A,0x4A,0x4A,0x7A,0x02,0x00,0x08,0x06,0x01,0x06,
    0x08,0x09,0x0A,0x08,0x0A,0x09,0x08,0x00};

const unsigned char lu1[]={
/*--  文字:  路  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x9E,0x12,0xF2,0x9E,0x48,0xC4,0xAB,0x92,0xAA,0xC6,0x40,0x00,0x0F,0x08,0x07,0x04,
	0x00,0x0F,0x04,0x04,0x04,0x0F,0x00,0x00};

const unsigned char sun1[]={
/*--  文字:  损  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x88,0xFF,0x48,0x00,0xF0,0x17,0x15,0xD5,0x15,0x17,0xF0,0x00,0x08,0x0F,0x00,0x00,
	0x09,0x04,0x02,0x01,0x02,0x04,0x09,0x00};

const unsigned char huai1[]={
/*--  文字:  坏  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x10,0x10,0xFF,0x10,0x80,0x42,0x22,0xFA,0x06,0x22,0xC2,0x00,0x04,0x04,0x03,0x02,
	0x00,0x00,0x00,0x0F,0x00,0x00,0x00,0x00};

const unsigned char tong1[]={
/*--  文字:  通  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x11,0xF2,0x00,0xF9,0xA9,0xAB,0xFD,0xAD,0xAB,0xF8,0x00,0x00,0x08,0x07,0x08,0x0B,
	0x08,0x08,0x0B,0x08,0x0A,0x0B,0x08,0x00};

const unsigned char xun1[]={
/*--  文字:  讯  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x10,0x11,0xF2,0x00,0x22,0xFE,0x22,0x22,0xFE,0x00,0x00,0x00,0x00,0x00,0x07,0x02,
	0x00,0x0F,0x00,0x00,0x03,0x04,0x0E,0x00};

const unsigned char zhong1[]={
/*--  文字:  中  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x00,0xF8,0x88,0x88,0x88,0xFF,0x88,0x88,0x88,0xF8,0x00,0x00,0x00,0x01,0x00,0x00,
	0x00,0x0F,0x00,0x00,0x00,0x01,0x00,0x00};

const unsigned char chang1[]={
/*--  文字:  常  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
	0x0C,0x04,0x75,0x56,0x54,0xD7,0x54,0x56,0x75,0x04,0x0C,0x00,0x00,0x07,0x01,0x01,
	0x01,0x0F,0x01,0x01,0x05,0x07,0x00,0x00};

const unsigned char Celsius[]={
/*--  文字:  ℃  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
	0x02,0x05,0x02,0xF8,0x04,0x02,0x02,0x02,0x04,0x0E,0x80,0x00,0x00,0x00,0x00,0x00,
    0x01,0x02,0x02,0x02,0x02,0x01,0x00,0x00};

const unsigned char Celsius1[]={0x06,0x09,0x09,0xE6,0xF8,0x0C,0x04,0x02,0x02,0x02,0x02,0x02,0x04,0x1E,0x00,0x00,
                       0x00,0x00,0x00,0x07,0x1F,0x30,0x20,0x40,0x40,0x40,0x40,0x40,0x20,0x10,0x00,0x00};/*"℃",0*/

const unsigned char rong1[]={
/*--  文字:  容  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
    0x46,0x42,0xAA,0xA6,0x92,0x8B,0x92,0xA6,0xAA,0x42,0x46,0x00,0x00,0x00,0x0F,0x04,
    0x04,0x04,0x04,0x04,0x0F,0x00,0x00,0x00};/*"容",0*/

//	12*12
const unsigned char dunhao1[]={0x00,0x00,0x00,0x00,0x00,0x60,0x60,0x00,0x00,0x00,0x00,0x00,
					  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};/*"．",0*/
const unsigned char tanhao1[]={0x00,0x00,0x00,0x00,0x00,0x3E,0x3E,0x00,0x00,0x00,0x00,0x00,
					  0x00,0x00,0x00,0x00,0x00,0x03,0x03,0x00,0x00,0x00,0x00,0x00};/*"！",0*/

const unsigned char liang1[]={0x08,0xFF,0xA9,0xA9,0xA9,0xFB,0xAD,0xA9,0xA9,0xFF,0x08,0x00,0x08,0x0A,0x0A,0x0A,
                     0x0A,0x0F,0x0A,0x0A,0x0A,0x0A,0x08,0x00};/*"量",0*/

const unsigned char ci1[]={0x02,0x04,0x80,0x20,0x18,0x87,0x74,0x84,0x04,0x14,0x0C,0x00,0x02,0x01,0x08,0x04,
                  0x02,0x01,0x00,0x01,0x02,0x04,0x08,0x00};/*"次",0*/

const unsigned char  shu4[]={0x48,0x2A,0x98,0x7F,0x28,0x4A,0x10,0xEF,0x08,0xF8,0x08,0x00,0x09,0x0B,0x05,0x05,
                   0x0B,0x00,0x08,0x05,0x02,0x05,0x08,0x00};/*"数",0*/
				   
const unsigned char  sheshidu2[]={
/*--  "℃"  --*/
/*--  此字体下对应的点阵为：宽x高=8x16   --*/
      0x00,0x18,0x14,0x18,0xF0,0x08,0x04,0x04,0x08,0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x0F,0x10,0x20,0x20,0x20,0x10,0x00,0x00}; /*"℃",0*/

const unsigned char fu1[]={
/*--  文字:  负  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
    0x08,0xF4,0x13,0x12,0x12,0xD2,0x1A,0x16,0x10,0xF0,0x00,0x00,0x08,0x09,0x04,0x04,0x02,
	0x01,0x02,0x02,0x04,0x05,0x08,0x00};/*"负",0*/

const unsigned char zai2[]={
/*--  文字:  载  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
    0x28,0xEA,0xBA,0xEF,0xAA,0xAA,0x08,0xFF,0x08,0xE9,0x0A,0x00,0x04,0x04,0x04,0x0F,0x02,
	0x02,0x08,0x04,0x03,0x04,0x0E,0x00};/*"载",0*/

const unsigned char she1[]={
/*--  文字:  设  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
    0x10,0x11,0xF2,0x00,0x50,0xCF,0x41,0x41,0x4F,0xD0,0x10,0x00,0x00,0x00,0x07,0x02,0x08,
	0x08,0x05,0x02,0x05,0x08,0x08,0x00};/*"设",0*/

const unsigned char bei1[]={
/*--  文字:  备  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
    0x20,0x24,0xD2,0x57,0x4A,0xCA,0x4A,0x56,0xD2,0x20,0x20,0x00,0x00,0x00,0x0F,0x05,0x05,
	0x07,0x05,0x05,0x0F,0x00,0x00,0x00};/*"备",0*/


const unsigned char ban1[]={
/*--  文字:  版  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
    0x00,0xFE,0x90,0x9F,0x10,0xFE,0x12,0xF2,0x12,0x91,0x71,0x00,0x08,0x07,0x00,0x0F,0x04,
	0x03,0x08,0x05,0x02,0x05,0x08,0x00};/*"版",0*/

const unsigned char ben1[]={
/*--  文字:  本 --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
    0x04,0x04,0x84,0x64,0x14,0xFF,0x14,0x64,0x84,0x04,0x04,0x00,0x02,0x01,0x02,0x02,0x02,
	0x0F,0x02,0x02,0x02,0x01,0x02,0x00};/*"本",0*/

const unsigned char hao1[]={
/*--  文字:  号 --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
    0x20,0x20,0xAF,0x69,0x29,0x29,0x29,0x29,0x2F,0x20,0x20,0x00,0x00,0x00,0x01,0x01,0x01,
	0x01,0x01,0x09,0x09,0x07,0x00,0x00};/*"号",0*/

const unsigned char ruan1[]={
/*--  文字:  软 --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=12x12   --*/
    0x64,0x5C,0xF7,0x44,0x20,0x18,0x07,0xE4,0x04,0x14,0x0C,0x00,0x02,0x02,0x0F,0x01,0x08,
	0x04,0x03,0x00,0x03,0x04,0x08,0x00};/*"软",0*/

const unsigned char  heiping[]={
/*--  文字: 充  --*/
/*--  宋体12;  此字体下对应的点阵为：宽x高=16x16   --*/
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

const unsigned char  dan1[] =	{0x00,0x7C,0x55,0x56,0x54,0xFC,0x54,0x56,0x55,0x7C,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x0F,0x01,0x01,0x01,0x01,0x01,0x00};/*"单",0*/
/* (12 X 12 , 宋体 )*/

const unsigned char cai1[]={0x12,0x32,0x52,0x17,0x32,0xD2,0x12,0x4F,0x2A,0x0A,0x02,0x00,0x09,0x09,0x05,0x03,0x01,0x0F,0x01,0x03,0x05,0x09,0x09,0x00};/*"菜",0*/
const unsigned char jian1[]={0x10,0xFC,0x03,0x90,0x8E,0x88,0x88,0xFF,0x88,0x88,0x88,0x00,0x00,0x0F,0x00,0x00,0x00,0x00,0x00,0x0F,0x00,0x00,0x00,0x00};/*"件",1*/

/* (12 X 12 , 新宋体 )*/





	#endif
