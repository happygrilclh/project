#ifndef APP_H__
#define APP_H__

#define CHARGE_HARD_VER		1.0
#define CHARGE_SOFT_VER		1.0

//0x55-电池充电，0xAA-电池维护，0x78-电容充电，0x88-操作完成，0x5A-待机，0xFF-状态异常

#define		CMD_STATE_BATTERY_CHARGE			0X01			    //电池标充界面
#define		CMD_STATE_OPERATE_FINISH			0x02				//操作完成
#define		CMD_STATE_INFO						0x03				//设备信息
#define		CMD_STATE_ABNORMAL					0x04					//状态异常




#define     DEVICE_TYPE_BATTERY_SIX				0X06
#define     DEVICE_TYPE_BATTERY_SEVEN			0X07
#define     DEVICE_TYPE_CAPACITANCE				0X00
#define     DEVICE_TYPE_MISSED_LOAD				0XFF

typedef   __packed struct
{
	uint8_t    start_flag;
	uint8_t    cmd;
	uint8_t    data_len;
	uint16_t   battery_current;
	uint16_t   battery_voltage;	
	uint16_t   abnormal_code;
	uint8_t    frame_sum;
	uint8_t    end_flag;
}DEVICE_STATE;

extern DEVICE_STATE g_refresh_device_state;
void analyze_comm_data(DEVICE_STATE *pDevice_State, uint8_t Size);
/*功能：1.帧格式校验； 2.命令解析*/
void check_frame_format(void);
#define BigLittleSwap16(x)  ( (((unsigned int)x & 0x00FF) << 8) | (((unsigned int)x & 0xFF00) >> 8) )
#define BigLittleSwap32(x)  ( (((unsigned long)x & 0x000000FF) << 24) | (((unsigned long)x & 0x0000FF00) << 8) | (((unsigned long)x & 0x00FF0000) >> 8) | (((unsigned long)x & 0xFF000000) >> 24))

#endif
