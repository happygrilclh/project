#include "global.h"
#include <string.h>
#include "FaultCode.h"

fault_t baFaultCode;

/*按优先级，故障显示界面*/
extern void fault_disp(uint8_t line_num,uint32_t mode);

void setFault(uint32_t CMD_faultCode)
{
	uint32_t fault_index =0;
	
	
	
	for(fault_index=0;fault_index<FAULTCODE_NUM;fault_index++)
	{
		if(mExistCode(CMD_faultCode,fault_index)) //检查收到的异常代码那一位被置了，即检查那些位有错误。
		{
		
			/*将异常错误码与优先级对应起来*/
			switch(BIT(fault_index))
			{
				case 0x01:
					mSetFaultCode(fault_charge_volt_HIGH);
					break;
				case 0x02:
					mSetFaultCode(fault_charge_volt_LOW);
					break;
				case 0x04:
					mSetFaultCode(fault_charge_current_HIGH);
					break;
				case 0x08:
					mSetFaultCode(fault_charge_current_LOW);
					break;
				case 0x10:
					mSetFaultCode(fault_output_shortCircuit);
					break;
				case 0x20:
					mSetFaultCode(fault_batts_connect_NG);
					break;
				case 0x40:
					mSetFaultCode(fault_batts_temp_HIGH);
					break;
				case 0x80:
					mSetFaultCode(fault_batts_temp_LOW);
					break;
				case 0x100:
					mSetFaultCode(fault_charge_comm_NG);
					break;
				
				default:
					break;
			}
		}
	}
	
}

/*按优先级查询故障号，显示故障界面，最多显示两个故障*/
//显示故障任务
void faultCode_task() 
{
	uint32_t pri_index = 0;  //优先级索引
	uint8_t disp_cnt = 1;
	
	for(pri_index=0;pri_index<FAULTCODE_NUM;pri_index++)
	{
			
		if(mExistFault(pri_index))
		{
			fault_disp((disp_cnt),(pri_index+1));
			mClrFaultCode(pri_index);
			
			disp_cnt++;
			if(disp_cnt>2)
			{
				baFaultCode.all = 0;
				disp_cnt=1;
			}
		}
	}
}
