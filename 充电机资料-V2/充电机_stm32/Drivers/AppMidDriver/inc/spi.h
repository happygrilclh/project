/**
  ******************************************************************************
  * File Name          : spi.h
  ******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __spi_H
#define __spi_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"


void SPI_Init(void);
void SPI_Write(uint8_t wr_data);


#ifdef __cplusplus
}
#endif

#endif 
