#include "global.h"
#include "string.h"
#include "spi.h"
#include "oled_ssd1309.h"
#include "drive_oled.h"


//命令模式，写命令到OLED 显示模块
void transfer_command(uint8_t data1)
{
	HAL_GPIO_WritePin(GPIOC, OLED_DC_Pin, GPIO_PIN_RESET);
	SPI_Write(data1);
}

//数据模式，写数据到OLED 显示模块
void transfer_data(int data1)
{
	HAL_GPIO_WritePin(GPIOC, OLED_DC_Pin, GPIO_PIN_SET);
	SPI_Write(data1);	
}

void lcd_address(uint8_t page,uint8_t column)
{
	column=column-1; //我们平常所说的第1列，在LCD驱动IC里是第0列。所以在这里减去1.
	page=page-1;
	transfer_command(0xb0+page); //设置页地址。每页是8 行。一个画面的64 行被分成8 个页。我们平常所说的第1 页，在LCD 驱动IC 里是第0 页，所以在这里减去1
	transfer_command(((column>>4)&0x0f)+0x10); //设置列地址的高4 位
	transfer_command(column&0x0f); //设置列地址的低4 位
}

//全屏清屏
void clear_full_screen(void)
{
	uint8_t i,j;
	for(j=0;j<8;j++)
	{
		lcd_address(1+j,1);
		for(i=0;i<128;i++)
		{
			transfer_data(0x00);
		}
	}
}

void OLED_Init(void)		 
{
	OLED_GPIO_init();
	transfer_command(0xFD);	//Command Lock  ( SSD1309 use, SSD1305 no use )
    transfer_command(0x12);	// Unlock Driver IC (0x12/0x16)	   

	transfer_command(0xAE);	// Set Display Off

	transfer_command(0xD5);	 //Set Display Clock Divide Ratio/Oscillator Frequency	  设置时钟分频
	transfer_command(0xA0);	//

	transfer_command(0xA8);	//Set Multiplex Ratio 	   设置多路复用比率
	transfer_command(0x3F);	// 

	transfer_command(0xD3);	//Set Display Offset
	transfer_command(0x00);	// 

	transfer_command(0x40);	// Set Display Start Line

    transfer_command(0xA1);	// Set SEG/Column Mapping (0xA0/0xA1)
	transfer_command(0xC8);	// Set COM/Row Scan Direction (0xC0/0xC8)

	transfer_command(0xDA);	//Set COM Pins Hardware Configuration
	transfer_command(0x12);	// Set Alternative Configuration (0x02/0x12)
	
	transfer_command(0x81);	//Set Current Control 
    transfer_command(0xFF);  // 
	
	transfer_command(0xD9);	//Set Pre-Charge Period 
	transfer_command(0x82);	// 

	transfer_command(0xDB);	//Set VCOMH Deselect Level
	transfer_command(0x34);	// 

    //transfer_command(0x20);
   // transfer_command(0x02);	// Set Page Addressing Mode (0x00/0x01/0x02)

	transfer_command(0xA4);	// Set Entire Display On/Off (0xA4/0xA5)
	transfer_command(0xA6);	// Set Normal/Inverse Display (0xA6/0xA7)

	clear_full_screen(); //清屏
	HAL_Delay(10);

	transfer_command(0xAF);		//Set Display On

	HAL_Delay(200);

}






