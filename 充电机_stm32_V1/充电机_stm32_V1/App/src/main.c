/************************************************
 mcu:		stm32f103zet6  512k FLASH, 64k RAM, 主频：72MHZ。
 项目功能：	该demo是用状态机做的到多菜单显示.
 特别说明：	此程序代码是本人闲暇时刻写的，只供个人学习，不做商用。
			此项目是基于原子的stm32（战舰板）板子做的项目，就是方便大家学习。

 uart：		通讯参数：波特率9600，8 位数据位，1 位停止位，无奇偶校验。 
 文件名称：	main.c
 功能：		
 版本：		V 1.0 
 作者：		happygril
************************************************/
/* Includes ------------------------------------------------------------------*/
#include "global.h"
#include "main.h"
#include "dma.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "key.h"
#include "oled_ssd1309.h"
#include "charge_fsm.h"
#include "drive_oled.h"

/* 函数声明--------------------------------------------------------------------*/
void SystemClock_Config(void);
void Hardware_Init(void);

int main(void)
{
	Hardware_Init();	
	
	while(1)  
	{
		 display_task();							//界面显示
		 scan_key();								//扫描按键
  	}
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_RCC_MCOConfig(RCC_MCO, RCC_MCO1SOURCE_PLLCLK, RCC_MCODIV_1);
  /** Enables the Clock Security System 
  */
  HAL_RCC_EnableCSS();
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/*
功能： 硬件初始化(时钟初始化，外设初始化，模块初始化)
*/
void Hardware_Init(void)
{
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();
  /* Initialize all configured peripherals */
  //MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM3_Init();
  /*串口波特率为9600*/
  MX_USART1_UART_Init();
	
  KEY_Init();				//初始化按键 
  OLED_Init();			   	//初始化LCD 

  uint8_t u = 0;
  HAL_UART_Transmit(&huart1, &u, 1,20);  //解决一开机闪屏的问题
}


 
